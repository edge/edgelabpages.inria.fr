---
title: Extended Formulations and Decomposition for GEneric optimization problems
keywords: ["Inria", "team", "optimization", "operations research", "extended formulations", "decomposition methods"]
---
<img src="resources/smoothing.webp" class="img-shadow" width="400px" style="float:right;margin:10px auto 20px 20px;">

Edge is a joint research team between the University of Bordeaux and Inria. 

Our research interests lie in the field of operations research (O.R.) and integer programming. Our goal is to propose mathematical methods and algorithms for abstract models that can be specialized to address a large number of problems. Our main methodological tools are based on decomposition methods and so-called extended integer programming formulations. Our expected contributions go from theoretical studies of problems and abstract models to algorithms and their efficient implementation in decision-support tools. To maximize our impact, we will develop high-level open- source interfaces for our methods, which will allow them to be used by a larger community of O.R. practitioners and decision makers. 

<br>

 <div class="row" id="logo">
   <div class="column" id="logo">
    <a href="https://www.inria.fr/en" target="_blank">
    <img src="resources/logos/Inria.webp" alt="Inria" width="180px">
    </a>
  </div>
  <div class="column" id="logo">
    <a href="https://www.math.u-bordeaux.fr/imb/" target="_blank">
    <img src="resources/logos/IMB.webp" alt="IMB" width="100px">
    </a>
  </div>
  <div class="column" id="logo">
    <a href="https://www.insmi.cnrs.fr/en" target="_blank">
    <img src="resources/logos/cnrs.webp" alt="cnrs" width="70px">
    </a>
  </div>
  <div class="column" id="logo">
    <a href="https://www.u-bordeaux.fr/en" target="_blank">
    <img src="resources/logos/UB.webp" alt="UB" width="180px">
    </a>
  </div>
</div> 

<br>
