# A folder to put the Edge team projects

## Organisation

One folder to store the past projects.

## Adding a project

Just copy-paste an existing one and change the information.

## Preview
The preview of each project is by default the title, the subtitle and the date. 
To this preview, you can also add a summary field in the first part of the file. If there is no summary field or if it is empty, then the first 70 characters of the job offer are displayed.

_Important: Everything after the tag "\<!--more-->" is not displayed_.

## Image
You can associate an image with the project. Use the attribute _imagesize_ to set the size of the image.

## Tags

You should give a unique value to the attribute _tag_.

For better indexing, you should add to the attribute _tags_ the list of tags of people participating to the project.

## Past projects

When the current year is strictly larger than the value set to the attribute "end", then the project is automatically put to the past project category.