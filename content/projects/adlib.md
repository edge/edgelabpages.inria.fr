---
title: ADLib
subtitle: A library for Aggregation/Disaggregation of large network flow models    
comments: false
tags: [project, clautiaux, detienne, errami, facq, froger, marques]
image: "../resources/pic_adlib.webp"
imagesize: 50
tag : adlib
date: 
weight: 

# optional info
consortium: Edge, LAAS, TBS
funding:  ANR Artificial Intelligence
start: 2022
end: 2026
summary: "We consider general aggregation/disaggregation techniques to address optimization problems that are expressed with the help of sequential decision processes. Our main goals are threefold: a generic formalism that encompasses the aforementioned techniques ; more efficient algorithms to control the aggregation procedures ; open-source codes that leverage and integrate these algorithms to efficiently solve hard combinatorial problems in different application fields."
---


<div style="float: right;max-width:600px;padding-right:50px;padding-left:50px;padding-bottom:20px">
<img src="../resources/pic_adlib.webp" alt="Network flow" class="img-title media-object" style="width: 60%;"/>
</div>


We consider general aggregation/disaggregation techniques to address optimization problems that are expressed with the help of sequential decision processes. Our main goals are threefold: a generic formalism that encompasses the aforementioned techniques ; more efficient algorithms to control the aggregation procedures ; open-source codes that leverage and integrate these algorithms to efficiently solve hard combinatorial problems in different application fields. 

We will jointly study two types of approaches, MIP and SAT, to reach our goals. MIP-based methods are useful to obtain proven optimal solutions, and to produce theoretical guarantees, whereas SAT solvers are strong to detect infeasible solutions and learn clauses to exclude these solutions. Their combination with CP through lazy clause generation is one of the best tools to solve highly combinatorial and non- linear problems. Aggregation/disaggregation techniques generally make use of many sub- routines, which allows an efficient hybridization of the different optimization paradigms. We also expect a deeper cross-fertilization between these different sets of techniques and the different communities.


__People involved :__
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- [Boris Detienne]({{< ref "/team-members/detienne" >}}) (Edge)
- [Najib Errami]({{< ref "/team-members/errami" >}}) (Edge)
- [Laurent Facq]({{< ref "/team-members/facq" >}}) (Edge)
- [Aurélien Froger]({{< ref "/team-members/froger" >}}) (Edge)
- [Luis Marques]({{< ref "/team-members/marques" >}}) (Edge)

---

{{< related_posts max=3 >}}
