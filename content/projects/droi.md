---
title: DROI
subtitle: "Primal and dual bounds for adjustable robust optimization" 
comments: false
tags: [project, arslan, detienne, flambard]
image: "../resources/pic_anr.webp"
imagesize: 25
tag : droi
date: 
weight: 

# optional info
consortium: 
funding:  ANR Junior Researcher
start: 2022
end: 2026
summary: 
---

Robust optimization has evolved as a key paradigm for handling data uncertainty within mathematical optimization problems: it
requires little historical information, can be used without characterizing probability distributions and often leads to tractable optimization problems that can be treated with existing deterministic optimization paradigms. However, the picture is more complex when some of the decisions (referred to as recourse decisions) can be adjusted after the uncertain data is known, to mitigate the effects of uncertainty, leading to adjustable robust optimization problems. Adjustable problems with discrete variables or multiple decision periods are particularly difficult to solve and, up to now, no scalable exact method has emerged. Project DROI proposes to study primal and dual approximations for such problems.

The goal of the project is two-fold: on the one hand, we will study novel decision rules in order to improve the quality and generality of applicability of the solutions proposed, on the other hand we will provide numerical quality measures on these solutions by developing corresponding duality tools. This will require studying the theoretical properties of the resulting optimization problems, and developing novel solution algorithms. Our methodological developments will be accompanied by numerical development and testing on applications that include the kidney exchange problem under compatibility uncertainty and the scheduling of nuclear reactors under outage duration uncertainty.

__People involved :__
- [Ayse N. Arslan]({{< ref "/team-members/arslan" >}}) (PI, Edge)
- [Boris Detienne]({{< ref "/team-members/detienne" >}})  (Edge)
- [Patxi Flambard]({{< ref "/team-members/flambard" >}}) (Edge)
- Merve Bodur (U. Toronto)
- Michael Poss (LIRMM, Montpellier)
- Jérémy Omer (INSA-Rennes).
