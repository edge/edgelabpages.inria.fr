---
title: PowDev
subtitle: "Proactive disaster response mitigation modeling and data-driven optimization for resilient power systems"
comments: false
tags: [project, clautiaux, detienne, arslan, powdev]
image: "../resources/powdev.webp"
imagesize: 80
tag : powdev
date: 
weight: 

# optional info
consortium: Centrale Supelec (project leader), Edge, UVSQ, University Grenoble Alpes, CNRS
funding:  PEPR TASE
start: 2024
end: 2029
summary:
---


**The project:** 

Strategic Power Systems Development for the Future (PowerDev), funded by PEPR TASE, studies optimization methods and reliability/resilience engineering applied to large-scale electrical power systems. The project is led by CentraleSupélec at the University of Paris Saclay and is composed of a consortium of higher education institutions across France (CentraleSupelec, UVSQ, University Grenoble Alpes), as well as research organizations (Inria, CNRS).

**Research topic and objectives:**

Modern power systems are expected to become increasingly complex to design and operate due to the growing number of renewable energy sources (RES). Renewable energy generation is, by nature, intermittent and introduces an amount of uncertainty that severely affects the physical responses of the power system, particularly in terms of voltage control and frequency regulation [1]. Moreover, RES integration within the power system requires the introduction of many new power electronic devices, which add to the system's complexity and increase its possible failure modes [2,3]. Combined with unexpected initiating events, these two main features can lead to cascading failure risks, triggering disastrous consequences to the power grid and, most notably, large-scale blackouts [4-7]. The economic and societal consequences to the impacted regions are usually massive, with economic loss measured in the tens of billions of dollars [8]. 
The main objective of this project is to evaluate and optimize the resilience of power systems in the context of a massive insertion of renewable energies. The project aims to elaborate a comprehensive and integrated set of decision support tools by considering extreme events in present and future climates, the complexity of the power grid, and socio-economic scenarios.

<div style="float: right;max-width:600px;padding:30px">
{{< figure src="../resources/powdev.webp">}}
</div>

Particularly, the PowerDev project seeks to achieve the following tasks:
1. Simulate, characterize, and analyze the scenario of a blackout in the case of a power system with massive renewable energy (PSMRE), including extreme weather events arising from climate change under present and future projections, realistic electrotech-nical and field knowledge, inherent grid complexity including interactions with other critical infrastructures, economics, and societal impact models.
2. Propose a quantitative and systemic framework to optimize the resilience of a PSMRE. Specifically focused on addressing possible remedial solutions to apply in the design and operational phases to maximize the system's resilience against major blackouts.


**Jobs:**

We are hiring a [PhD student]({{<  ref "jobs/2024/phd_powdev/"  >}}), a postdoctoral researcher (2024 or 2025), and an engineer (2024 or 2025).


__People involved:__
- [Ayse Arslan]({{<  ref "team-members/froger.md"  >}})  (Edge)
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Edge)
