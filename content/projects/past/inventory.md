---
title: Inventory Routing
subtitle: Branch-and-cut-and-price for inventory routing problems   
comments: false
tags: [project]
image: "../../resources/inventory.webp"
imagesize: 70
tag : inventory
date: 
weight: 

# optional info
consortium: Edge
funding:  Région Nouvelle Aquitaine / Inria
start: 2019
end: 2022
summary:
---

<div style="float: right;max-width:600px;padding:30px">
<img src="../../resources/inventory.webp" class="img-title media-object" style="width: 100%;"/>
</div>

One way to improve delivery efficiency is to allow the supplier to manage customer inventory itself. 
This makes it possible to deliver at the most relevant times, while ensuring a minimum level of stock. 
In this case, the supplier makes replenishment decisions for products based on specific inventory and supply chain policies. 
This practice is often described as a win-win model: suppliers save on distribution and production costs because they can coordinate shipments for their different customers, 
and buyers benefit from a better service cost, and can outsource its inventory management, which is not necessarily its core business. 
In such contexts, the supplier must make three simultaneous decisions: (1) when to serve a given customer, 
(2) how much to deliver to that customer when served, and 
(3) how to combine customers into vehicle routes. In the operations research literature, we speak of the “Inventory Routing Problem” (IRP).



The project concerned the practical resolution of IRP type problems using integer linear programming techniques. As part of this project, we recruited Isaac Balster for a doctoral thesis.

People involved :
- [Ruslan Sadykov]({{<  ref "team-members/past/sadykov.md"  >}}) (Edge)
- [Isaac Balster]({{< ref "/team-members/balster" >}}) (Edge)

