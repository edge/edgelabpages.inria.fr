---
title: DDROP
subtitle: Decision Dependent Robust Optimization
comments: false
tags: [project, detienne, arslan]
image: "../resources/pic_anr.webp"
imagesize: 25
tag : ddrop
date: 
weight: 

# optional info
consortium: Edge, ESSEC Business School, LIRMM
funding:  ANR - PRC
start: 2024
end: 2029
summary:
---


Many real-life decision-making problems under uncertainty include some form of interaction between the actions of the decision-maker and the realization of uncertain parameters. For instance, in medical appointment scheduling, no-shows of the patients are typically related to the schedules themselves whereas, in long-term medical treatments, the state of a patient evolves depending on past medication (decision-dependent uncertainty). On the other hand, in planning organ transplants, the uncertainty related to the compatibility of donors and patients needs to be investigated via expensive medical tests (information discovery) under a given budget constraint.
It is therefore essential to incorporate the interactions of the decision-maker with the uncertain parameters within optimization under uncertainty models. This project proposes the study and solution of robust optimization models formalizing such interactions under a single mathematical framework with decision-dependent uncertainty sets. 

The principal objective of this project is to advance the theoretical understanding and numerical solution of robust optimization problems with decision-dependent uncertainty sets. These problems have mostly been neglected in the literature despite their theoretical and applied interest, leaving many research questions open. This project aims to respond to some of these questions and fill some of the outlined gaps in the literature by studying the complexity and solution algorithms for these problems, exploring their connections with classical paradigms such as bilevel programming and adjustable robust optimization and developing solution algorithms based on reformulations, decomposition algorithms, and machine learning. Any progress made in this sense should interest both researchers and practitioners from multiple communities that do not always interact with each other. 

__People involved :__
- [Boris Detienne]({{< ref "/team-members/detienne" >}}) (PI, Edge)
- Laurent Alfandari (ESSEC Business School)
- [Ayse N. Arslan]({{< ref "/team-members/arslan" >}}) (Edge)
- Ivana Ljubic (ESSEC Business School)
- Michael Poss (LIRMM)
- Emiliano Traversi (ESSEC Business School)

---

{{< related_posts max=3 >}}
