---
title: Our projects
subtitle: List of ongoing projects involving our team members
layout : list_tag_withpast
past_title : Completed projects
list_tag : [project]
---

<!-- 
the list of projects is generated automatically 

When adding a new project make sure to add the tag

tag: 'project'
-->
