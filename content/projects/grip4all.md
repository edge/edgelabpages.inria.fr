---
title: Grip4all
subtitle: Robotic palletizing of heterogeneous products without prior scheduling
comments: false
tags: [project, grip4all, clautiaux, paul-archipczuk]
image: "../resources/grip4all.webp"
imagesize: 50
tag : grip4all
date: 
weight: 

# optional info
consortium: Inria Edge, Inria Auctus, PPRIME, FIVES, KMSD 
funding:  Défi Robotique
start: 2025
end: 2028
summary:
---


<div style="float: right;max-width:600px;padding-right:50px;padding-left:50px;padding-bottom:20px">
<img src="../resources/grip4all.webp" alt="Workflow" class="img-title media-object" style="width: 60%;"/>
</div>

The aim of the Grip4All project is to make industry more competitive by developing a new palletising cell adapted to the severe constraints imposed on the logistics flow when handling mixed products (of varying dimensions and weight) and arranging them on a pallet, without having to sort them manually upstream. This new type of palletising meets a strong demand from a number of sectors, notably mass distribution and the food industry. It meets the demand for handling heterogeneous products without imposing constraints on their packaging, which significantly improves productivity and eliminates tedious human tasks. No similar solution currently exists on the market. The flexible robotics issues addressed will be transposable to other logistics processes in the factory of the future.
  
Our objective is to develop algorithms for the optimised design of palletising plans. Two main levers are considered: the order of arrival of items and the design of pallet loading plans. Given a list of items with physical properties and assigned to orders and a location, and a list of pallets, we aim to calculate an order of arrival for each item and to place the items on the pallets by considering two criteria: the number of pallets used and the pallet balance score. Several technical issues need to be considered. Firstly, the efficiency of the system will be highly dependent on the order of arrival of the items. The order is constrained by the general organisation of the warehouse: several configurations will be evaluated. Secondly, the pallet construction algorithms will be adapted to the gripping system chosen. We will implement an approach based on the team's expertise in several combinatorial optimisation tools: mathematical programming (MIP), heuristics, constraint programming (PPC) and the combination of optimisation algorithms with machine learning tools. 


__People involved at EDGE :__
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- [Paul Archipczuk]({{< ref "/team-members/paul-archipczuk" >}}) (Edge)

---

{{< related_posts max=3 >}}
