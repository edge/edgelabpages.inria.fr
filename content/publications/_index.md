---
title: Our publications
subtitle: List of the publications authored by EDGE members
comments: false

---


{{< publications hal_id_institute="1089098" start_year="2020">}}

{{% center %}}
[Publications of our former research group RealOpt]({{< ref "/publications-realopt" >}} "")
{{%/ center %}}


