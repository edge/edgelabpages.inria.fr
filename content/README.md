# Download the website locally on your machine
You need to clone the git repository with the following command line
```
git clone git@gitlab.inria.fr:edge/edge.gitlabpages.inria.fr.git
```

# Hugo
To preview the website on your machine after modifying/adding content, you should have Hugo installed on it [(click here)](https://gohugo.io/installation/)

At the root of the git folder, execute the following command line
```
hugo server --disableFastRender
```
Click then on the link that is displayed to see the website on your browser.

If you run into errors or if some of your modifications seem to have no effect, a good advice is to close the window, stop hugo (Ctrl + C on Linux), and then execute the two following command lines.
```
hugo mod clean
hugo server --disableFastRender
```
Sometimes, you may also need to clean the cache of your web browser (especially if you modify the css).



# Add new content or modify content

Open the README.md file at the root of the folder where you want to add content.
* [Add a news (seminar, new result, PhD defense, ...) or modify an existing news](news/README.md)
* [Add a new team member or modify my member page](team-members/README.md)
* [Add a new job offer or modify an existing offer](jobs/README.md)
* [Add a new project or modify an existing project](project/README.md)
* [Add a new collaboration or modify an existing collaboration](collaboration/README.md)
* [Add a new software or modify an existing software](software/README.md)


