---
title: "VRPSolver"
tags: [ software ]
subtitle: Branch-and-price + Resource Constrained Shortest Path Pricing  # optional

# optional parameters
tag: software_vrpsolver # unique tag of this project
date:
weight: 10

# job posting specific and optional
type: 
duration: 2021-2024
info_url: https://vrpsolver.math.u-bordeaux.fr/
active: true  # if false it will the offer will be displayed in 'past jobs'

summary:
---


**Description**

VRPSolver is a Branch-Cut-and-Price based exact solver for vehicle routing and some related problems. <!--more-->
 <br/>
Main use cases of the solver are:
- benchmarking heuristic algorithms against the lower bound/optimal solution obtained by the solver;
- benchmarking exact algorithms against the solver;
- creating efficient models for new vehicle routing problems;
- testing new families of (robust) cutting planes within a state-of-the-art Branch-Cut-and-Price algorithm.

<!--more-->

**Status.** Available for academics.

 <br/>
 
{{% center %}}
<a href="https://vrpsolver.math.u-bordeaux.fr/" role="button" class="btn btn-primary">Visit VRPsolver website</a>
{{%/ center %}}
