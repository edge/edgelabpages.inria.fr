---
title: "Benders by Batch"
tags: [ software, clautiaux, froger, blanchot ]
subtitle: Benders decomposition for two-stage stochastic linear problems  # optional

# optional parameters
tag: software_bbb # unique tag of this project
date:
weight: 20

# job posting specific and optional
info_url: https://bapcod.math.u-bordeaux.fr/
active: true  # if false it will the offer will be displayed in 'past jobs'

summary:
---

**Description** 

Repository containing a C++ implementation of the Benders by batch algorithm described in the article:

Xavier Blanchot, François Clautiaux, Boris Detienne, Aurélien Froger, Manuel Ruiz. (2023). The Benders by batch algorithm: design and stabilization of an enhanced algorithm to solve multicut Benders reformulation of two-stage stochastic programs. *European Journal of Operational Research*. DOI: [10.1016/j.ejor.2023.01.004](https://doi.org/10.1016/j.ejor.2023.01.004) 

<!--more-->

**Language.** C++

**Status.** Open source

{{% center %}}
<a href="https://gitlab.inria.fr/edge/benders-by-batch" role="button" class="btn btn-primary">Visit the repository</a>
{{%/ center %}}
