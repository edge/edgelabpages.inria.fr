---
title: "VRPSolverEasy"
tags: [ software ]
subtitle: A Python interface to VRPSolver  # optional

# optional parameters
tag: software_vrpsolver # unique tag of this project
date:
weight: 10

# job posting specific and optional
type: PhD theis
duration: 2021-2024
info_url: https://vrpsolver.math.u-bordeaux.fr/
active: true  # if false it will the offer will be displayed in 'past jobs'

summary:
---

**Description**

VRPSolverEasy is a simple interface for [VRPSolver]({{< ref "VRPSolver.md" >}}), which is a state-of-the-art Branch-Cut-and-Price exact solver for vehicle routing problems. <!--more-->
 <br/>
 As a price to pay for the simplicity, this interface is restricted to some standard VRP variants, which involve the following features and their combinations:
 
 - capacitated vehicles,
- customer time windows,
- heterogeneous fleet,
- multiple depots,
- open routes,
- optional customers with penalties,
- parallel links to model transition time/cost trade-off,
- incompatibilities between vehicles and customers,
- customers with alternative locations and/or time windows.


**Status.** The VRPSolverEasy package itself is open-source and free to use. It includes compiled libraries of [BaPCod]({{< ref "VRPSolver.md" >}}), its VRPSolver extension, and COIN-OR CLP solver. These libraries are also free to use.

For better performance, it is possible to use VRPSolverEasy together with CPLEX MIP solver. This combination called academic version requires an access to the source code of BaPCod available with an academic-use-only license. The academic version of VRPSolverEasy additionally includes a MIP-based (slow) heuristic which is useful for finding feasible solutions in the absence of an external heuristic solver.



{{% center %}}
<a href="https://github.com/inria-UFF/VRPSolverEasy" role="button" class="btn btn-primary">Visit VRPSolverEasy GitHub repository</a>
{{%/ center %}}
