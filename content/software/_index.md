---
title: Our softwares
subtitle: List of softwares developed by EDGE
layout : list_tag # layout which enables past/ongoing separation

past_title : Old software (not maintained anymore) # the title of past topics

list_tag : [software] # the tag used for all job postings
---
{{% center %}}
People at Edge develop generic and application-specific optimization softwares. 
{{%/ center %}}

<!--x 
Everything before these tags will be displayed before the list of job offers and everyting below ward will be displayed after the list
x-->


