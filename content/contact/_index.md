---
title: Contact
---

## Administrative Assistant 
[Marie-Melissandre Roy]({{< ref "/team-members/roy" >}})<br>

## Our locations 	

<div class = "row" >

<div class="col-md-4 col-sm-6">

### IMB
Institut de Mathématiques de Bordeaux <br>
Université de Bordeaux, Bâtiment A33 <br> 
Cours de la Libération <br>
33400 Talence

<img src="./resources/imb.webp" style="float:left" >

</div>

<div class="col-md-8 col-sm-6">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2830.5798857407135!2d-0.5956940227233175!3d44.80974977686469!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd55277837c025f9%3A0x93476ed002d48214!2sIMB%2C%20Institut%20de%20Math%C3%A9matiques%20de%20Bordeaux!5e0!3m2!1sfr!2sfr!4v1700121139127!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

</div>
</div>


<div class = "row" >

<div class="col-md-4 col-sm-6">

### INRIA
EDGE, Inria Bordeaux – Sud-Ouest, <br>
200 Avenue de la Vieille Tour, <br>
33405 Talence, France

<img src="./resources/inria.webp"  style="float:left">

</div>

<div class="col-md-8 col-sm-6">

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19042.629192229913!2d-0.6117865617140973!3d44.80722309202546!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa3ab5d733522c459!2sCentre%20de%20Recherche%20Inria%20Bordeaux%20-%20Sud-Ouest!5e0!3m2!1sfr!2sfr!4v1646989096163!5m2!1sfr!2sfr" style="border:0;height:450px;width:100%" allowfullscreen="" loading="lazy"></iframe>

</div>
</div>
