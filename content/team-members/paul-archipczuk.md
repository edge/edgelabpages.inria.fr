---
### obligatory fields
tags: [team, grip4all, paul-archipczuk]  # you can add more tags if needed, but team has to be there
role: engineer # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Paul Archipczuk # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Engineer, Inria # your position
tag: paul-archipczuk  # your tag, used to link you to news, publications, projects, ....
interests: "Software engineering, optimization" # your interests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: "" # phone number 
email: paul.archipczuk@inria.fr # your contact email
location: "IMB - Université de Bordeaux" # your office location
image: ../resources/paul-archipczuk.webp
imagesize: 30
---



<!--more-->

---

{{< related_posts max=3 >}}
