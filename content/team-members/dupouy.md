---
### obligatory fields
tags: [team, clautiaux ]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Cécile Dupouy # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, Kedge BS # your position
tag: dupouy  # your tag, used to link you to news, publications, projects, ....
interests: "Supply-chain, physical internet" # your interests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: "" # phone number 
email:  # your contact email
location: "Kedge BS" # your office location
image: ../resources/cecile.webp
imagesize: 30
---

<!--more-->

**PhD title**: Operational Urban Delivery problem with consolidated parcels and synergized transportation options

**Supervisors** : 
Walid Klibli (Kedge BS), [François Clautiaux]({{<  ref "team-members/clautiaux.md"  >}}) (Edge), Nicolas Labarthe

<br/>

---

{{< related_posts max=3 >}}
