---
### obligatory fields
tags: [team, arslan, detienne]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Patxi Flambard # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, Inria # your position
tag: flambard  # your tag, used to link you to news, publications, projects, ....
interests: "Robust optimization" # you rinterests
webpage: https://www.researchgate.net/profile/Patxi-Flambard # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email: patxi.flambard@math.u-bordeaux.fr # your contact email
location: "IMB - Université de Bordeaux - Bureau 269" # your office location
image: ../resources/flambard.webp
imagesize: 30
---

<!--more-->

**Supervisors**
- [Ayse Arslan]({{<  ref "team-members/arslan.md"  >}}) (Edge)
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Edge)


---

##### **Publications**

{{< publications hal_id="patxi-flambard" start_year="2023">}}

---

{{< related_posts max=3 >}}
