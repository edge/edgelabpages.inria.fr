---
### obligatory fields
tags: [team, adlib, clautiaux, froger]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Luis Marques # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Ph.D student, Inria # your position
tag: marques  # your tag, used to link you to news, publications, projects, ....
interests: "Combinatorial optimization, mathematical programming, aggregation-disaggregation techniques" # your interests
webpage: https://luismarques.me # your website link, or something like linkedin, researchgate, ....
phone: "" # phone number 
email: luis.lopes-marques@u-bordeaux.fr # your contact email
location: "IMB - Université de Bordeaux - Bureau 265" # your office location
image: ../resources/Luis.webp
imagesize: 30
---
<!--more-->

**Ph.D title**: Aggregation-disaggregation techniques for solving large network-flow models

**Supervisors**: [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge) and [Aurélien Froger]({{< ref "/team-members/froger" >}}) (Edge)


##### **Objective of the thesis**

We consider general aggregation/disaggregation techniques to address optimization problems that are expressed with the help of sequential decision processes. Our main goals are threefold: a generic formalism that encompasses the aforementioned techniques ; more efficient algorithms to control the aggregation procedures ; open-source codes that leverage and integrate these algorithms to efficiently solve hard combinatorial prcodeoblems in different application fields.

We will jointly study two types of approaches, MIP and SAT, to reach our goals. MIP-based methods are useful to obtain proven optimal solutions, and to produce theoretical guarantees, whereas SAT solvers are strong to detect infeasible solutions and learn clauses to exclude these solutions. Their combination with CP through lazy clause generation is one of the best tools to solve highly combinatorial and non- linear problems. Aggregation/disaggregation techniques generally make use of many sub-routines, which allows an efficient hybridization of the different optimization paradigms. We also expect a deeper cross-fertilization between these different sets of techniques and the different communities.


---

##### **Publications**

{{< publications hal_id="luis-marques" start_year="2020">}}

---

{{< related_posts max=3 >}}
