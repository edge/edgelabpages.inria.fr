---
title: Our team
subtitle: Meet our team members 
layout : list_team
list_tag : team
roles: [permanent,admin,engineer, external,postdoc,phd,intern]
role_names: ["Permanent Researchers","Administrative staff", "Engineers", "External Collaborators", "Post-Docs", "PhD Students", "Interns"]

---

<!--x
 team list goes here - don't remove 
- everything before will be shown before the list of team members
- everything after will be shown after teh list of team members
-  {{< figure src="./resources/team_decembre_2023.jpg" class="text-center " caption="The Edge team, 2023">}}
x-->

<h3 style="text-decoration-line: underline; margin-top: 20px;"> EDGE alumni members </h3>
{{< accordion >}}
    {{< accordion_entry name="Researchers" team="Edge">}}
        <!-- Researcher and external collaborators -->
        {{< alumnis group="permanent" >}}
        {{< alumnis group="external" >}}
    {{</ accordion_entry>}}
    {{< accordion_entry name="PostDocs/Engineers" team="Edge">}}
        <!-- PostDocs and Engineers  -->
        {{< alumnis name="Eduardo QUEIROGA" affiliation="Inria">}}
        {{< alumnis group="postdoc" >}}
        {{< alumnis group="engineer" >}}
    {{</ accordion_entry>}}
    {{< accordion_entry name="PhD Students" team="Edge">}}
        <!-- Phd students -->
        {{< alumnis group="phd" >}}
    {{</ accordion_entry>}}
    {{< accordion_entry name="Interns" team="Edge">}}
        <!-- Interns -->
        {{< alumnis name="Henri LEFEBVRE">}}
        {{< alumnis name="Yanyu ZHOU">}}
        {{< alumnis group="intern" >}}
    {{</ accordion_entry>}}
{{</ accordion >}}

<h3 style="text-decoration-line: underline; margin-top: 20px;"> RealOpt alumni members </h3>

{{< accordion >}}
    {{< accordion_entry name="Researchers" team="Realopt">}}
        <!-- Researcher and external collaborators -->
        {{< alumnis name="François VANDERBECK " >}}
		{{< alumnis name="Gautier STAUFFER " >}}
		{{< alumnis name="Arnaud PECHER " >}}
		{{< alumnis name="Andrew MILLER " >}}
		{{< alumnis name="Lionel EYRAUD-DUBOIS " >}}
		{{< alumnis name="Olivier BEAUMONT " >}}
        {{< alumnis group="permanent_realopt" >}}
    {{</ accordion_entry>}}
    {{< accordion_entry name="PostDocs/Engineers" team="Realopt">}}
        <!-- PostDocs and Engineers  -->
        {{< alumnis name="Halil SEN" >}}
        {{< alumnis name="Agnes LEROUX" >}}
        {{< alumnis group="postdoc_realopt" >}}
        {{< alumnis group="engineer_realopt" >}}
    {{</ accordion_entry>}}
    {{< accordion_entry name="PhD Students" team="Realopt">}}
        <!-- Phd students -->
        {{< alumnis name="Mohamed BENKIRANE">}}
        {{< alumnis name="Imen BEN MOHAMED">}}
        {{< alumnis name="Rodolphe GRISET">}}
        {{< alumnis name="Gaël GUILLOT">}}
        {{< alumnis name="Jérémy GUILLOT">}}
        {{< alumnis name="Guillaume MARQUES">}}
        {{< alumnis name="Orlando RIVERA LETELIER">}}
        {{< alumnis name="Quentin VIAUD">}}
        {{< alumnis group="phd_realopt" >}}
    {{</ accordion_entry>}}
{{</ accordion >}}

