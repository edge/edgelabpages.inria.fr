---
### obligatory fields
tags: [team, arslan, detienne, froger]  # you can add more tags if needed, but team has to be there
role: postdoc # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Mariam Sangare # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Postdoctoral researcher, Inria # your position
tag: sangare  # your tag, used to link you to news, publications, projects, ....
interests: # your interests
webpage: # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: "IMB - UB" # your office location
image: ../resources/mariam.webp
imagesize: 30
---

<!--more-->

**Supervisors**
- [Ayse Arslan]({{<  ref "team-members/arslan.md"  >}}) (Edge)
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Edge)
- [Aurélien Froger]({{<  ref "team-members/froger.md"  >}}) (Edge)


---

##### **Publications**

{{< publications hal_id="mariam-sangare" start_year="2020">}}

---

{{< related_posts >}}
