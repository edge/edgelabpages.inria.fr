---
### obligatory fields
tags: [team, detienne]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Michael Gaury # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, Kedge BS # your position
tag: gaury  # your tag, used to link you to news, publications, projects, ....
interests: "Operations Research" # your interests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: "" # phone number 
email:  # your contact email
location:  # your office location
image: 
imagesize: 30
---
<!--more-->

**Supervisors**
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Edge)

---

{{< related_posts max=3 >}}
