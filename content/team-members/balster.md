---
### obligatory fields
tags: [team, sadykov ]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Isaac Balster # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, Inria # your position
tag: balster  # your tag, used to link you to news, publications, projects, ....
interests: "Column generation, vehicle routing, inventory routing, branch-and-cut-and-price" # your interests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: "" # phone number 
email:  # your contact email
location:  # your office location
image: 
imagesize: 30
---

<!--more-->
**Supervisors**
- [Ruslan Sadykov]({{<  ref "team-members/past/sadykov.md"  >}}) (Edge)

---

##### **Publications**

{{< publications hal_id="1451558" start_year="2020">}}

---

{{< related_posts max=3 >}}
