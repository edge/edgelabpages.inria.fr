---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: admin # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Marie-Melissandre Roy  # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Administrative Assistant, Inria # your position
tag:  # your tag, used to link you to news, publications, projects, ....
interests:  # you rinterests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: "Inria Bordeaux, 200 avenue de la Vieille Tour, Talence, France" # your office location
image: 
imagesize: 30
---

<!--more-->

