---
### obligatory fields
tags: [team,alumni]  # you can add more tags if needed, but team has to be there
role: admin # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Joëlle Lacoste Rodrigues # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Administrative Assistant, Inria (until 2024) # your position
tag:  # your tag, used to link you to news, publications, projects, ....
interests:  # you rinterests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: "Inria Bordeaux, 200 avenue de la Vieille Tour, Talence, France / ENSC Building, Hall Est" # your office location
# your image can be a link from the internet or can be placed the the resources folder 
image:
imagesize: 30
---

<!--more-->

