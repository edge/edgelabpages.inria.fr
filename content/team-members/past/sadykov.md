---
### obligatory fields

tags: [team, alumni]  # you can add more tags if needed, but team has to be there
role: permanent # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Ruslan SADYKOV # your name

# optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Inria (until 2023)
tag: sadykov # your tag, used to link you to news, publications, projects, ....
interests: branch-and-price, vehicle routing, scheduling # your interests
webpage: https://www.math.u-bordeaux.fr/~rsadykov/ # your webiste link, or something like linkedin, researchgate, ....
phone:  # phone number 
email:  
location: # your office location
# your image can be a link from the internet or can be placed the the resources folder  


---


<!--more-->
##### **Publications**
{{< publications hal_id="ruslan-sadykov" start_year="1995">}}

---

{{< related_posts >}}
