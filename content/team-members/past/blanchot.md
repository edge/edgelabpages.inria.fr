---
### obligatory fields

tags: [team,alumni,rte]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Xavier BLANCHOT # your name

# optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD (2022), RTE # your position
tag: blanchot # your tag, used to link you to news, publications, projects, ....
interests: Benders Decomposition, Bi-level optimization # your interests
webpage: https://www.linkedin.com/in/xavier-blanchot-067216229/ # your webiste link, or something like linkedin, researchgate, ....
phone:  # phone number 
email:  
location: # your office location
# your image can be a link from the internet or can be placed the the resources folder  
image: 
imagesize: 30
---

<!--more-->
{{< related_posts max=3 >}}
