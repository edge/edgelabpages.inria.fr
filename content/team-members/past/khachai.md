---
### obligatory fields
tags: [team, alumni, sadykov]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Daniil KHACHAI # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD (2023), Kedge BS # your position
tag: khachai  # your tag, used to link you to news, publications, projects, ....
interests: "integer linear programming" # your interests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: "" # phone number 
email: # your contact email
location: "Kedge BS" # your office location
image: 
imagesize: 30
---
 
<!--more-->
**Supervisors**
- [Ruslan Sadykov]({{<  ref "team-members/past/sadykov.md"  >}}) (Edge)

---

{{< related_posts max=3 >}}
