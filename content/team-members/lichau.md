---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Sylvain Lichau # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, Université de Bordeaux # your position
tag: lichau  # your tag, used to link you to news, publications, projects, ....
interests: "Vehicle routing" # your interests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: "" # phone number 
email:  # your contact email
location: "IMS - UB" # your office location
image: 
imagesize: 30
---

<!--more-->
**Supervisors**
- [Ruslan Sadykov]({{<  ref "team-members/past/sadykov.md"  >}}) (Edge)

---

##### **Publications**

{{< publications hal_id="sylvain-lichau" start_year="2020">}}

---

{{< related_posts max=3 >}}
