---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: permanent # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Ayse N. Arslan # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Scientific Researcher, Inria # your position
tag: arslan  # your tag, used to link you to news, publications, projects, ....
interests: # you rinterests
webpage: https://aysnrarsln.github.io/ # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  ayse-nur.arslan@inria.fr # your contact email
location: IMB - Université de Bordeaux - A33 - Bureau 266 # your office location
image: ../resources/Ayse.webp 
imagesize: 30
---

<!--more-->

- Principal investigator of [**ANR DROI**]({{< ref "/projects/droi" >}}) 
- Responsible for young researcher targeted actions, GDR-ROD
- Member of the scientific comitee, GDR-ROD
- Co-responsible for scientific animation, axe DOR, GDR-ROD


---


<i class="fa fa-university" aria-hidden="true"></i> **Research**  | [ResearchGate](https://www.researchgate.net/profile/Ayse-Arslan-7) |
<br>
 <i class="fa fa-key" aria-hidden="true"></i> **Keywords:** *Mathematical programming, Robust optimization, Stochastic programming, Decomposition methods, Polyhedral approaches*
<br>
<i class="fa fa-industry" aria-hidden="true"></i> **Main industrial Collaborations** | EDF |
<br>
<i class="fa fa-graduation-cap" aria-hidden="true"></i> **PhD Students**
- [**Patxi Flambard**]({{< ref "/team-members/flambard" >}}) - Primal and dual bounds for adjustable robust optimization
- **Théo Guyard** - Représentations parcimonieuses dans des dictionnaires continus pour le diagnostic automatique de maladies du foie

---

##### **Publications**

{{< publications hal_id="ayse-arslan" start_year="2000">}}

---

{{< related_posts >}}
