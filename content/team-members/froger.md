---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: permanent # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Aurélien Froger # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Maître de conférences, Université de Bordeaux # your position
tag: froger  # your tag, used to link you to news, publications, projects, ....
interests:  # you rinterests
webpage: https://www.math.u-bordeaux.fr/~afroger001/  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: IMB - Université de Bordeaux - A33 # your office location
image: ../resources/aurelien.webp 
imagesize: 30
---

<!--more-->

<i class="fa fa-university" aria-hidden="true"></i> **Research:** [ORCID](https://orcid.org/0000-0002-1814-9087) | [Scopus](https://www.scopus.com/authid/detail.uri?authorId=57193672335)
<br>
 <i class="fa fa-book" aria-hidden="true"></i> **Methodologies:** *Mathematical programming, Mixed integer linear programming, Benders decomposition, Dantzig-Wolfe reformulation, Cut generation algorithm, Dynamic programming, Matheuristics, Metaheuristics*
<br>
 <i class="fa fa-caret-square-o-right" aria-hidden="true"></i> **Application areas:** *Combinatorial optimization problems in the field of logistics and energy (Routing, Scheduling, Design, ...)*
<br>
<i class="fa fa-industry" aria-hidden="true"></i> **Industrial collaborations:**  EDF | RTE | CATIE
<br>
<i class="fa fa-graduation-cap" aria-hidden="true"></i> **PhD Students**
- [**Luis Marques**]({{< ref "/team-members/marques" >}}) - Aggregation-disaggregation techniques in integer linear programming algorithms
- [**Fulin Yan**]({{< ref "/team-members/yan" >}}) - Machine learning and optimization 
- **Past** - [Xavier Blanchot]({{< ref "/team-members/past/blanchot" >}}) (2018-2022) - Benders decomposition and bilevel optimization 


---

##### **Publications**

{{< publications hal_id="aurelien-froger" start_year="2016">}}

---

{{< related_posts >}}
