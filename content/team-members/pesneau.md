---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: permanent # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Pierre Pesneau # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Maître de conférences, Université de Bordeaux # your position
tag: pesneau  # your tag, used to link you to news, publications, projects, ....
interests: "Polyhedral analysis, optimization" # you rinterests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: IMB - Université de Bordeaux - A33 # your office location
image: ../resources/Pierre.webp 
imagesize: 30
---



<!--more-->
##### **Publications**
{{< publications hal_id="pierre-pesneau" start_year="1995">}}

---

{{< related_posts >}}
