---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: permanent # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: François Clautiaux # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Team leader and Professor, Université de Bordeaux  # your position
tag: clautiaux # your tag, used to link you to news, publications, projects, ....
interests:  integer programming, extended formulations, operations research, cutting and packing problems # you rinterests
webpage: https://www.math.u-bordeaux.fr/~fclautia/ # your webiste link, or something like linkedin, researchgate, ....
email: francois.clautiaux@inria.fr # your contact email
location: "IMB - Université de Bordeaux - A33" # your office location
# your image can be a link from the internet or can be placed the the resources folder  
image: ../resources/francois.webp
imagesize: 30
weight: 
---

<!--more-->

- Coordinator of UB2030 - **CAP IA program in Artificial Intelligence**
- Head of the master program in Operations Research in Bordeaux
- Principal investigator of **ANR AD-LIB** 
- Former president of [ROADEF](https://www.roadef.org)
- Board member of Domex IA / data science

---



<i class="fa fa-university" aria-hidden="true"></i> **Research**  | [Scholar google](https://scholar.google.fr/citations?user=TFLLaO4AAAAJ&hl=fr) |
<br>
 <i class="fa fa-key" aria-hidden="true"></i> **Keywords:** *Integer programming, Extended formulations, Column generation, Network-flow models, Operations Research, Cutting and packing, ...*
<br>
<i class="fa fa-industry" aria-hidden="true"></i> **Main industrial Collaborations** | CATIE | RTE | EDF |  Saint-Gobain | Renault | SNCF | Vekia/Asis | Thales 
<br>
<i class="fa fa-graduation-cap" aria-hidden="true"></i> **PhD Students**
- **Pierre Pinet** - Stochastic vehicle routing problems
- **Parfait Ametana** - Robust network design problems
- **Cécile Dupouy** - Logistics and physical internet
- **Luis Marques** - Aggregation-disaggregation techniques in integer linear programming algorithms
- **Fulin Yan** - Machine learning and optimization 
- **Past** | Ali Khanafer (2007-2010) - packing problems with conflicts | Nadia Dahmani (2009-2014) - multi-objective packing problems | Matthieu Gérard (2012-2015) - staff scheduling | Jérémy Guillot (2013-2018) - clustering problems | Quentin Viaud (2014-2018) - 2D packing problems |  Gaël Guillot (2017-2020) - Aggregation-disaggregation techniques in dynamic programming algorithms | Mohamed Benkirane - Optimization of train operations | [Xavier Blanchot]({{< ref "/team-members/past/blanchot" >}}) (2018-2022) - Benders decomposition and bilevel optimization 


---

##### **Publications**

{{< publications hal_id="clautiaux" start_year="2000">}}

---

{{< related_posts >}}


