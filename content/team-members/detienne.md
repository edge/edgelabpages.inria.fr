---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: permanent # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Boris Detienne # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Maître de conférences, HDR, Université de Bordeaux # your position
tag: detienne  # your tag, used to link you to news, publications, projects, ....
interests: "Robust optimization" # you rinterests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email: # your contact email
location: IMB - Université de Bordeaux - A33 # your office location
image: ../resources/Boris.webp 
imagesize: 30
---


<!--more-->
##### **Publications**

{{< publications hal_id="boris-detienne" start_year="2000">}}

---

{{< related_posts >}}
