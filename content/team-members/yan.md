---
### obligatory fields
tags: [team,  clautiaux, froger]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Fulin Yan # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, CATIE # your position
tag: yan  # your tag, used to link you to news, publications, projects, ....
interests: "Optimization, machine learning, deep learning, reinforcement learning" # you rinterests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: "IMB - UB"  # your office location
image: 
imagesize: 30
---

<!--more-->

**Supervisors**
- [François Clautiaux]({{<  ref "team-members/clautiaux.md"  >}}) (Edge)
- [Aurélien Froger]({{<  ref "team-members/froger.md"  >}}) (Edge)

**PhD title**: Solving combinatorial optimization problems using hybrid methods that combine machine learning techniques with existing optimization techniques

##### **Objective of the thesis**

We wish to develop heuristic approaches based on hybrid methods to solve problems formulated using Sequential decision processes (SDP). One difficulty is that these approaches only have implicit knowledge of the problem to be solved (via oracles for example). When the problem is very large, it is not possible to generate all of the possible states, and the proposed methods must use exploration phases.

Another objective of the thesis is to provide efficient procedures for the techniques which are used in the resolution of SDP based on successive relaxations of the state space: in particular the oracle which allows to choose the states to be aggregated or disaggregated.

---

##### **Publications**

{{< publications hal_id="fulinyan-210" start_year="2021">}}


---

{{< related_posts >}}
