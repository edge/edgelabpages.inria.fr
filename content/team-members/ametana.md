---
### obligatory fields
tags: [team, detienne, clautiaux]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Parfait Ametana # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, Université de Bordeaux # your position
tag: ametana  # your tag, used to link you to news, publications, projects, ....
interests: "Robust optimization, disaster management" # you rinterests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location:  # your office location
image:  
imagesize: 30
---
<!--more-->

**PhD title**: Disaster management and robust optimization

**Supervisors** : [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Edge)
- Olga Battaia (Kedge BS), [François Clautiaux]({{<  ref "team-members/clautiaux.md"  >}}) (Edge), Mehdi Amiri-Ahref

<br/>


---

##### **Publications**

{{< publications hal_id="komlanvi-parfait-ametana" start_year="2020">}}

---

{{< related_posts >}}
