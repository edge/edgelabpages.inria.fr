---
### obligatory fields
tags: [team]  # you can add more tags if needed, but team has to be there
role: engineer # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Laurent Facq # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: Research Engineer, CNRS # your position
tag: facq  # your tag, used to link you to news, publications, projects, ....
interests: "" # you rinterests
webpage: "https://www.math.u-bordeaux.fr/imb/fiche-personnelle?uid=lfacq" # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: "IMB - Université de Bordeaux - A33" # your office location
image: ../resources/laurent.webp
imagesize: 30
---


<!--more-->
##### **Publications**

{{< publications hal_id="laurent-facq" start_year="1969">}}

---

{{< related_posts >}}
