---
### obligatory fields
tags: [team,  clautiaux, arslan]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: Pierre Pinet # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student, St-Gobain Recherche # your position
tag: pierre-pinet  # your tag, used to link you to news, publications, projects, ....
interests: "Optimization, Vehicle Routing" # you rinterests
webpage:  # your webiste link, or something like linkedin, researchgate, ....
phone: # phone number 
email:  # your contact email
location: "IMB - UB"  # your office location
image: ../resources/pierre-pinet.webp
imagesize: 30
---

<!--more-->

**Supervisors**
- [François Clautiaux]({{<  ref "team-members/clautiaux.md"  >}}) (Edge)
- [Ayse Arslan]({{<  ref "team-members/arslan.md"  >}}) (Edge)

**PhD title**: "Routing under uncertainty"

##### **Objective of the thesis**

Solving routing problems under uncertainty.

---

{{< related_posts >}}
