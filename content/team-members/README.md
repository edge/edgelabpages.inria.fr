# Team Members

## Creating a new member

To create a new team member add a new file to this repository with a name of your choice : for example `my_name.md`.

Once you have created it you can copy paste his code in the md file and modify the contents and the metadata.

```yaml
---
### obligatory fields
layout: team_member_page # not to be changed
tags: [team]  # you can add more tags if needed, but team has to be there
role: phd # your role in the team [permanent, postdoc, external, phd, admin, student, engineer]
title: John Doe # your name

#### optional fields
# if they are not set they will not be displayed on your personal page
affiliation: PhD student  # your position
tag: jdoe  # your tag, used to link you to news, publications, projects, ....
interests: robotics, control, humans # your scientific interests
webpage: https://johndoe.com # your webiste link, or something like linkedin, researchgate, ....
phone: "+3312345678" # phone number 
email: john.doe@inria.fr # your contact email
location: ENSC Bordeaux # your office location
# your image can be a link from the internet or can be placed the the resources folder 
image: ../resources/image.png or https://johndoe.com/image.png
imagesize: 30
---


<!---
Your page content goes here. 
Text format is pure markdown. 
You can also use html. 
Do not remove the line with <!--more--> below.
-->
<!--more-->

Welcome to my page

```


#
