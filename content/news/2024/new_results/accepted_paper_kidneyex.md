---
title: "KidneyExchange.jl: A Julia package for solving the kidney exchange problem with branch-and-price"
tags: [paper, newresult, 2024, news, arslan, yan]
comments: false
image: 
imagesize: 
date: 2024-01-19
weight : 
layout : paper
authors:  Ayse N. Arslan, Jeremy Omer, Fulin Yan
keywords:  Kidney exchange problem, Column-generation and Branch-and-Price, Decomposition algorithms, Julia Language
abstract : "The kidney exchange problem (KEP) is an increasingly important healthcare management problem in most European and North American countries which consists of matching incompatible patient-donor pairs in a centralized system. Despite the significant progress in the exact solution of KEP instances in recent years, larger instances still pose a challenge especially when altruistic donors are taken into account. In this article, we present a branch-and-price algorithm for the exact solution of KEP in the presence of altruistic donors. This algorithm is based on a disaggregated cycle and chains formulation where subproblems are managed through graph copies. We additionally, present a branch-and-price algorithm based on the position-indexed chain-edge formulation, as well as two compact formulations. We formalize and analyze the complexity of the resulting pricing problems and identify the conditions under which they can be solved using polynomial-time algorithms. We propose several algorithmic improvements for the branch-and-price algorithms as well as for the pricing problems. We extensively test all of our implementations using a benchmark made up of different types of instances with the largest instances considered having 10000 pairs and 1000 altruists. Our numerical results show that the proposed algorithm can be up to two orders of magnitude faster compared to the state-of-the-art. All models and algorithms presented in the paper are gathered in an open-access Julia package, KidneyExchange.jl."
---


##### **Manuscript**

The manuscript is published in <i>Mathematical Programming Computation</i> (DOI: <a href="https://doi.org/10.1007/s12532-023-00251-7">10.1007/s12532-023-00251-7</a>).
<br/>
{{% center %}}
<a href="https://rdcu.be/dwnC9" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



