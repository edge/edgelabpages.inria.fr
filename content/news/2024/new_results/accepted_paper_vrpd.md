---
title: A Competitive Heuristic Algorithm for Vehicle Routing Problems with Drones
tags: [paper, newresult, 2024, news, paper, froger, vrpd]
comments: false
image: ../../resources/vrpd.webp
imagesize: 45
date: 2024-05-28
weight:
layout: paper 
authors:  Xuan Ren, Aurélien Froger, Ola Jabali, Gongqian Liang
keywords: Drones, Dynamic programming, Variable neighborhood search, Routing, Heuristics
abstract: "We propose a heuristic algorithm capable of handling multiple variants of the vehicle routing problem with drones (VRPD). Assuming that the drone may be launched from a node and recovered at another, these variants are characterized by three axes, (1) minimizing the transportation cost or minimizing the makespan, (2) the drone is either allowed or not allowed to land while awaiting recovery, and (3) single or multiple trucks each equipped with a drone. In our algorithm, we represent a VRPD solution as a set of customer sequences and evaluate it via local search procedures solving for each sequence a problem that we refer to as the fixed route drone dispatch problem (FRDDP). Given a sequence of customers to be served by a single truck and its drone, the FRDDP selects a subset of customers to be served by the drone and determines drone launch and recovery nodes, while ensuring that each such customer is positioned between two nodes in the initial sequence. We introduce a heuristic dynamic program (HDP) to solve the FRDDP with reduced computational complexity compared to an exact solution algorithm for the problem. We reinforce our algorithm by developing filtering strategies based on the HDP. We benchmark the performance of our algorithm on nine benchmark sets pertaining to four VRPD variants resulting in 932 instances. Our algorithm computes 651 of 680 optimal solutions and identifies 189 new best-known solutions."
---


##### **Manuscript**

The manuscript is published in <i>European Journal of Operational Research</i> (DOI: <a href="https://doi.org/10.1016/j.ejor.2024.05.031">10.1016/j.ejor.2024.05.031</a>).
<br/>
{{% center %}}
<a href="https://inria.hal.science/hal-04010250" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



