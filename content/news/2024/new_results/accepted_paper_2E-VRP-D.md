---
title: A Branch-Cut-And-Price approach for the Two-Echelon Vehicle Routing Problem with Drones
tags: [paper, newresult, 2024, news, lichau]
comments: false
image: 
imagesize: 100
date: 2024-11-07
weight:
layout: paper 
authors:  Sylvain Lichau,  Ruslan Sadykov, Julien François, Rémy Dupas
keywords: Vehicle routing, branch-cut-and-price, drones
abstract: "In this paper, we propose a new set-partitioning model for the two-echelon vehicle routing problem with drones (2E-VRP-D), where partial routes corresponding to drone movements are enumerated using an efficient dynamic program. To solve the model we use an exact branch-cut-and-price algorithm, and a labelling algorithm for the pricing problem both based on state-of-the art literature. We also propose an adaptation of the wellknown rounded capacity cuts for this problem, as well as pre-processing methods to reduce the size of the problem. In addition, this paper presents an effective heuristic branch-cut-and-price, based on the exact branch-cut-and-price algorithm. Extensive computational experiments are presented, showing that the exact algorithm can solve all the instances from the literature on the 2E-VRP-D, and almost multiply by four the size of the clustered instances solved optimally. Sensitivity analysis is also conducted for the proposed improvements."
---




##### **Manuscript**

The manuscript is published in <i>Computers and Operations Research</i> (DOI: <a href="https://doi.org/10.1016/j.cor.2024.106869">10.1016/j.cor.2024.106869</a>).
<br/>
{{% center %}}
<a href="https://hal.science/hal-04584429v2/document" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



