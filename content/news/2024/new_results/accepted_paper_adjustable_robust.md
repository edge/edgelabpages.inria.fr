---
title: Adjustable robust optimization with objective uncertainty
tags: [paper, newresult, 2024, news, detienne]
comments: false
image: 
imagesize: 
date: 2024-01-01
weight : 
layout : paper
authors: Boris Detienne, Henri Lefebvre, Enrico Malaguti, Michele Monaci 
keywords: Uncertainty modelling, Two-stage robust optimization, Reformulation, Fenchel duality, Branch-and-price
abstract: "In this work, we study optimization problems where some cost parameters are not known at decision time and the decision flow is modeled as a two-stage process within a robust optimization setting. We address general problems in which all constraints (including those linking the first and the second stages) are defined by convex functions and involve mixed-integer variables, thus extending the existing literature to a much wider class of problems. We show how these problems can be reformulated using Fenchel duality, allowing to derive an enumerative exact algorithm, for which we prove asymptotic convergence in the general case, and finite convergence for cases where the first-stage variables are all integer. An implementation of the resulting algorithm, embedding a column generation scheme, is then computationally evaluated on a variant of the Capacitated Facility Location Problem with uncertain transportation costs, using instances that are derived from the existing literature. To the best of our knowledge, this is the first approach providing results on the practical solution of this class of problems."
---


##### **Manuscript**

The manuscript is published in <i>European Journal of Operational Research</i> (DOI: <a href="https://doi.org/10.1016/j.ejor.2023.06.042">10.1016/j.ejor.2023.06.042</a>).
<br/>
{{% center %}}
<a href="https://inria.hal.science/hal-04235571" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



