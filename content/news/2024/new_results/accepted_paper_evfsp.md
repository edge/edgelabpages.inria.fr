---
title: A Continuous Approximation Model for the Electric Vehicle Fleet Sizing Problem
tags: [paper, newresult, 2024, news, froger, evfsp]
comments: false
image: ../../resources/evfsp.webp
imagesize: 100
date: 2024-05-03
weight:
layout: paper 
authors: Brais González-Rodrı́guez, Aurélien Froger, Ola Jabali, Joe Naoum-Sawaya
keywords: Continuous Approximation, Electric Vehicles, Fleet Sizing, Region Partitioning
abstract: "Establishing the size of an EV fleet is a vital decision for logistics operators. In urban settings, this
issue is often dealt with by partitioning the geographical area around a depot into service zones, each
served by a single vehicle. Such zones ultimately guide daily routing decisions. We study the problem
of determining the optimal partitioning of an urban logistics area served by EVs. We cast this problem
in a Continuous Approximation (CA) framework.

</br>
Considering a ring radial region with a depot at its center, we introduce the electric vehicle fleet
sizing problem (EVFSP). As the current range of EVs is fairly sufficient to perform service in urban
areas, we assume that the EV fleet is exclusively charged at the depot, i.e., en-route charging is not
allowed. In the EVFSP, we account for EV features such as limited range, and non-linear charging and
energy pricing functions stemming from Time-of-use (ToU) tariffs. Specifically, we combine non-linear
charging functions with pricing functions into charging cost functions, establishing the cost of charging
an EV for a target charge level. We propose a polynomial time algorithm for approximating this
function and prove that the resulting approximation is exact under certain conditions. The resulting
function is non-linear with respect to the route length. Therefore, we propose a Mixed Integer Non-
linear Program (MINLP) for the EVFSP, which optimizes both dimensions of each zone in the partition.
We strengthen our formulation with symmetry breaking constraints. Furthermore, considering convex
charging cost functions, we show that zones belonging to the same ring are equally shaped. We propose
a tailored MINLP formulation for this case. Finally, we derive upper and lower bounds for the case of
non-convex charging cost functions.

</br>
We perform a series of computational experiments. Our results demonstrate the effectiveness of
our algorithm in computing charging cost functions. We show that it is not uncommon that these
functions are non-convex. Furthermore, we observe that our tailored formulation for convex charging
cost functions improves the results compared to our general formulation. Finally, contrary to the
results obtained in the CA literature for combustion engine vehicles, we empirically observe that the
majority of EVFSP optimal solutions consist of a single inner ring."
---




##### **Manuscript**

The manuscript is published in <i>Mathematical Programming</i> (DOI: <a href="https://doi.org/10.1007/s10107-024-02141-9">10.1007/s10107-024-02141-9</a>).
<br/>
{{% center %}}
<a href="https://www.math.u-bordeaux.fr/~afroger001/documents/EVFSP-v3-HAL.pdf" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



