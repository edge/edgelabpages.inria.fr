---
title: Uncertainty reduction in static robust optimization
tags: [paper, newresult, 2024, news, arslan, droi]
comments: false
image: 
imagesize: 
date: 2024-06-20
weight : 
layout : paper
authors:  Ayse N. Arslan, Michael Poss
keywords:  Combinatorial optimization, Robust optimization, NP-hardness, Reformulation
abstract: "Uncertainty reduction has recently been introduced in the robust optimization literature as a relevant special case of decision-dependent uncertainty. Herein, we identify two relevant situations in which the problem is polynomially solvable. We provide insights into possible MILP reformulations and illustrate the practical relevance of our theoretical results on the shortest path instances from the literature."
---


##### **Manuscript**

The manuscript is published in <i>Operations Research Letters</i> (DOI: <a href="https://doi.org/10.1016/j.orl.2024.107131">10.1016/j.orl.2024.107131</a>).
<br/>
{{% center %}}
<a href="https://hal.science/hal-04158877" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



