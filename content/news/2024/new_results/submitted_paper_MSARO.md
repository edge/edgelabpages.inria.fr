---
title: Two-stage and Lagrangian Dual Decision Rules for Multistage Adaptive Robust Optimization
tags: [paper, newresult, 2024, news, arslan, droi]
comments: false
image: 
imagesize: 
date: 2024-09-06
weight:
layout: paper 
authors:  Maryam Daryalal, Ayse N. Arslan, Merve Bodur
keywords: Optimization under uncertainty, Robust optimization, Decision rules
abstract: "In this work, we design primal and dual bounding methods for multistage adaptive robust optimization (MSARO) problems motivated by two decision rules rooted in the stochastic programming literature. From the primal perspective, this is achieved by applying decision rules that restrict the functional forms of only a certain subset of decision variables resulting in an approximation of MSARO as a two-stage adjustable robust optimization problem. We leverage the two-stage robust optimization literature in the solution of this approximation. From the dual perspective, decision rules are applied to the Lagrangian multipliers of a Lagrangian dual of MSARO, resulting in a two-stage stochastic optimization problem. As the quality of the resulting dual bound depends on the distribution chosen when developing the dual formulation, we define a distribution optimization problem with the aim of optimizing the obtained bound and develop solution methods tailored to the nature of the recourse variables. Our framework is general-purpose and does not require strong assumptions such as a stage-wise independent uncertainty set, and can consider integer recourse variables. Computational experiments on newsvendor, location-transportation, and capital budgeting problems show that our bounds yield considerably smaller optimality gaps compared to the existing methods."
---


##### **Preprint**

The preprint is open access and can be found in HAL database.
<br/>
{{% center %}}
<a href="https://hal.science/hal-04090602/document" role="button" class="btn btn-primary">Access the preprint</a>
{{%/ center %}}
<br/>



