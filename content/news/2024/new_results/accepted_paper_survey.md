---
title: "Last fifty years of integer linear programming: a focus on recent practical advances"
tags: [paper, newresult, 2024, news, clautiaux]
comments: false
image: 
imagesize: 
date: 2024-11-25
weight:
layout: paper 
authors:   François Clautiaux , Ivana Ljubić
keywords: Combinatorial Optimization, Mixed-Integer Linear Programming, Branch-and-Cut, Dantzig-Wolfe Decomposition, Benders Decomposition
abstract: "Mixed-integer linear programming (MILP) has become a cornerstone of operations research. This is
driven by the enhanced efficiency of modern solvers, which can today find globally optimal solutions within
seconds for problems that were out of reach a decade ago. The versatility of these solvers allowed successful
applications in many areas, such as transportation, logistics, supply chain management, revenue management, finance, telecommunications, and manufacturing. Despite the impressive success already obtained,
many challenges remain, and MILP is still a very active field.

This article provides an overview of the most significant results achieved in advancing the MILP solution
methods. Given the immense literature on this topic, we made deliberate choices to focus on computational
aspects and recent practical performance improvements, emphasizing research that reports computational
experiments. We organize our survey into three main parts, dedicated to branch-and-cut methods, Dantzig-
Wolfe decomposition, and Benders decomposition. The paper concludes by highlighting ongoing challenges
and future opportunities in MILP research."
---


##### **Preprint**

The manuscript is published in <i>European Journal of Operational Research</i> (DOI: <a href="https://doi.org/10.1016/j.ejor.2024.11.018">10.1016/j.ejor.2024.11.018</a>) and is open access.
<br/>
{{% center %}}
<a href="https://doi.org/10.1016/j.ejor.2024.11.018" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



