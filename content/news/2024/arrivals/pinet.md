---
title: "Pierre Pinet joins EDGE"
tags: [ news, Arrival, pierre-pinet]
date: 2024-12-01            # very important
subtitle: CIFRE PhD student with Saint-Gobain

# optional parameters
tag: # unique tag of this project
image: ../../../../team-members/resources/pierre-pinet.webp
imagesize: 20
---

We are happy to announce that [Pierre Pinet]({{<  ref "team-members/pierre-pinet.md"  >}}) has joined our team as a CIFRE PhD student with Saint-Gobain. Pierre will work on [solving routing problems under uncertainty]({{<  ref "collaborations/saintgobain2024.md"  >}}).
