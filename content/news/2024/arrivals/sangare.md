---
title: "Mariam Sangare joins EDGE"
tags: [ news, Arrival, sangare ]
date: 2024-10-01            # very important
subtitle: Postdoctoral researcher within the EDF-Inria Challenge

# optional parameters
tag: # unique tag of this project
image: ../../../../team-members/resources/mariam.webp
imagesize: 20
---

We are happy to announce that [Mariam Sangare]({{<  ref "team-members/sangare.md"  >}}) has joined our team as a postdoctoral researcher. Mariam will work on the [EDF-Inria Challenge]({{<  ref "collaborations/defi_inria_edf.md"  >}})  on generation expansion planning problems. Prior to her arrival in the team Mariam has completed her PhD thesis at University of Montpellier under the supervision of Michael Poss.
