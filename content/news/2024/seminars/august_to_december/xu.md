---
title: Towards an exact framework for quantum state separable problem
speaker: Liding Xu
tags: [seminar, seminars_2024_aug_dec]
date: 2024-11-28           # very important
publishdate: 2024-08-01
layout: seminar


# optional parameters
institution: Zuse Institute Berlin
position: Postdoctoral researcher
website: https://lidingxu.github.io/
tag: xu_seminar2024
image: 
imagesize: 
---

Separable states are multipartite quantum states that can be written as a convex combination of product states. Product states are multipartite quantum states that can be written as a tensor product of states in each space. Quantum state separable problem is an NP-hard problem but fundamental for quantum information theory. We propose two relaxation techniques for this problem. In the view of commutative optimization, we treat the states as matrices of multilinear complex polynomials. Our relaxation technique is found similar to that for complex bilinear polynomials arising in the Alternating Current Optimal Power Flow problem. In the view of non-commutative optimization, we treat the states as tensor products of bounded Positive Semi-definite variables. We propose a generalized McCormick relaxations using linear matrix inequalities. These two relaxations will be the key component to drive an exact branch-and-cut algorithm.
