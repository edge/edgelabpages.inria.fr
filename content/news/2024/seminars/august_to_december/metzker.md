---
title: Optimisation distributionnellement robuste pour le problème de dimensionnement de lots multi-produits sous incertitude du rendement de la production 
speaker: Paula Metzker
tags: [seminar, seminars_2024_aug_dec]
date: 2024-12-19           # very important
publishdate: 2024-08-01
layout: seminar


# optional parameters
institution: HEC Montréal
position: Postdoctoral researcher
website: 
tag: metzker_seminar2024
image: 
imagesize: 
---

Cette recherche est menée pour examiner une approche d'optimisation distributionnellement robuste appliquée au problème de dimensionnement de lots avec des retards de production et une incertitude de rendement sous des ensembles d'ambiguïté par événement. Les ensembles d'ambiguïté basés sur les moments, Wasserstein et le clustering K-Means sont utilisés pour représenter la distribution des rendements. Des stratégies de décision statiques et statiques-dynamiques sont également considérées pour le calcul d'une solution. Dans cette présentation, la performance de différents ensembles d'ambiguïté sera présentée afin de déterminer un plan de production qui soit satisfaisant et robuste face aux changements de l'environnement. Il sera montré, à travers une expérience numérique, que le modèle reste traitable pour tous les ensembles d'ambiguïté considérés et que les plans de production obtenus demeurent efficaces pour différentes stratégies et contextes décisionnels.
