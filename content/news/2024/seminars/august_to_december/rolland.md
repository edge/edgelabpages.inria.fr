---
title: Management of a battery on short term electricity markets 
speaker: Lionel Rolland
tags: [seminar, seminars_2024_aug_dec]
date: 2024-09-18           # very important
publishdate: 2024-08-01
layout: seminar


# optional parameters
institution: ENSTA Paris
position: Master's student - M2 MPRO
website: 
tag: rolland_seminar2024
image: 
imagesize: 
---



Our work aims to quantify the benefit of storage flexibilities such as a battery on several short term electricity markets. We especially focus on two different markets, the intraday market (ID) and the activation market of the automatic Frequency Restoration Reserve (aFRR), also known as the secondary reserve. We propose algorithms to optimize the management of a small battery (<= 5 MWh) on these markets. In this talk, we first present the modeling of the problem, then we show some theoretical results and numerical simulations. We conclude by listing some limitations of the method.
