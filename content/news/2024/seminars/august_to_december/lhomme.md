---
title: Preuves computationnelles pour algorithmes aléatoires online pour le online bin stretching 
speaker: Antoine Lhomme
tags: [seminar, seminars_2024_aug_dec]
date: 2024-08-29           # very important
publishdate: 2024-08-01
layout: seminar


# optional parameters
institution: Grenoble-INP
position: PhD student
website: 
tag: lhomme_seminar2024
image: 
imagesize: 
---


Un paradigme usuel pour l'étude d'algorithmes online est l'analyse compétitive, qui correspond à étudier le pire cas d'un algorithme sur toutes ses entrées possibles. Cependant, une telle analyse peut parfois être trop grossière pour être réellement intéressante. Pour contourner ce problème, il est possible de considérer des algorithmes aléatoires - c'est-à-dire des algorithmes dont le comportement est aléatoire. Cet exposé présentera des méthodes computationnelles pour étudier les algorithmes aléatoires dans le problème classique du online bin stretching, à partir d'outils de théorie des jeux et de programmation linéaire. 

Ce problème est un problème de packing où un algorithme doit placer une séquence d'objets (reçue de manière online) dans des boîtes sans contraintes de capacité; l'objectif est de minimiser le volume occupé de la boîte la plus remplie. La séquence d'objets reçue en entrée est de plus garantie de rentrer dans les boîtes si ces dernières avaient capacité unitaire. Nous verrons en particulier comment construire des bornes inférieures sur la performance d'algorithmes online aléatoires pour le problème du online bin stretching à partir de programmes linéaires.

