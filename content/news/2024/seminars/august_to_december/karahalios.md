---
title: "Column Elimination: An Iterative Approach to Solving Integer Programs"
speaker: Anthony Karahalios
tags: [seminar, seminars_2024_aug_dec]
date: 2024-11-06           # very important
publishdate: 2024-08-01
layout: seminar


# optional parameters
institution: Carnegie Mellon University
position: PhD student
website: 
tag: karahalios_seminar2024
image: 
imagesize: 
---

Column elimination is an exact algorithm to solve discrete optimization problems via a 'column' formulation in which variables represent a combinatorial structure such as a machine schedule or truck route.  Column elimination works with a relaxed set of columns, from which infeasible ones are iteratively eliminated.  As the total number of columns can typically be exponentially large, we use relaxed decision diagrams to compactly represent and manipulate the set of columns.  In this talk, we provide an introduction to the column elimination method and present recent algorithmic improvements resulting in competitive performance on large-scale vehicle routing problems. Specifically, our approach closes a large vehicle routing benchmark instance with 1,000 locations for the first time.


