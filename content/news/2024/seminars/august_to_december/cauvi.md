---
title: "Landmark Hub Labeling: Improved Bounds and Faster Query Answering"
speaker: Justine Cauvi
tags: [seminar, seminars_2024_aug_dec]
date: 2024-11-29           # very important
publishdate: 2024-08-01
layout: seminar


# optional parameters
institution: ENS Lyon
position: Student
website: 
tag: cauvi_seminar2024
image: 
imagesize: 
---

Hub Labeling (HL) is a state-of-the-art method for answering shortest-distance queries between node pairs in weighted graphs. It provides very fast query times but also requires considerable additional space to store the label information. Recently, a generalization of HL, called Landmark Hub Labeling (LHL), has been proposed, that conceptionally allows a storage of fewer label information without compromising the optimality of the query result. However, query answering with LHL was shown to be slower than with HL, both in theory and practice. Furthermore, it was not clear whether there are graphs with a substantial space reduction when using LHL instead of HL. 

In this talk, we describe a new way of storing label information of an LHL such that query times are significantly reduced and then asymptotically match those of HL. We establish novel bounds between different labeling variants and provide a comparative experimental study between approximation algorithms for HL and LHL. We demonstrate that label sizes in an LHL are consistently smaller than those of HL across diverse benchmark graphs, including road networks.
