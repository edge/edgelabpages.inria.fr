---
title: Decision aid for tactical transportation problems 
speaker: Guillaume Joubert
tags: [seminar, seminars_2024_aug_dec]
date: 2024-10-31           # very important
publishdate: 2024-08-01
layout: seminar

# optional parameters
institution: LS2N, IMT Atlantique
position: Postdoctoral researcher
website: 
tag: joubert_seminar2024
image: 
imagesize: 
---

Due to the complexity of real-world planning processes addressed by major transportation companies, decisions are often made considering subsequent problems at the strategic, tactical, and operational planning phases. However, these problems still prove to be individually very challenging. This talk will present two examples of tactical transportation problems motivated by industrial applications: the Train Timetabling Problem (TTP) and the Service Network Scheduling Problem (SNSP). The TTP aims at scheduling a set of trains, months to years before actual operations, at every station of their path through a given railway network while respecting safety headways. The SNSP determines the number of vehicles and their departure times on each arc of a middle-mile network while minimizing the sum of vehicle and late commodity delivery costs. For these two problems, the consideration of capacity and uncertainty in travel times are discussed. We present models and solution approaches including MILP formulations, Tabu search, Constraint Programming techniques, and a Progressive Hedging metaheuristic.
