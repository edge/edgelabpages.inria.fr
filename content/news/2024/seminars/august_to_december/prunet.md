---
title: "Decision-focused learning: theory, applications and recent algorithmic developments"
speaker: Thibault Prunet
tags: [seminar, seminars_2024_aug_dec]
date: 2024-12-12           # very important
publishdate: 2024-08-01
layout: seminar


# optional parameters
institution:  Ecole des Ponts ParisTech
position: Postdoctoral researcher
website: 
tag: prunet_seminar2024
image: 
imagesize: 
---

Real-world industrial applications frequently confront the task of decision-making under uncertainty. The classical paradigm for these complex problems is to use both machine learning (ML) and combinatorial optimization (CO) in a sequential and independent manner. ML learns uncertain quantities based on historical data, which are then used used as an input for a specific CO problem. Decision-focused learning is a recent paradigm, which directly trains the ML model to make predictions that lead to good decisions. This is achieved by integrating a CO layer in a ML pipeline, which raises several theoretical and practical challenges.

In this talk, we aim at providing a comprehensive introduction to decision-focused learning. We will first introduce the main challenges raised by hybrid ML/CO pipelines, the theory of Fenchel-Young losses for surrogate differentiation, and the main applications of decision-focused learning. As a second objective, we will present our ongoing works that aim at developing efficient algorithms based on the Bregman geometry to address the minimization of the empirical regret in complex stochastic optimization problems.

