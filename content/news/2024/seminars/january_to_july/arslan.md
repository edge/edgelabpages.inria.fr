---
title: A tutorial on decision rules for robust adjustable optimization
speaker: Ayse Nur Arslan
tags: [seminar, arslan, seminars_2024_jan_july]
date: 2024-05-30            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: Inria-Bordeaux
position: Junior Researcher
website: https://edge.gitlabpages.inria.fr/team-members/arslan/
tag: arslan_seminar2024 # unique tag of this project
image: 
imagesize: 
---

Adjustable robust optimization problems, as a subclass of multi-stage optimization under uncertainty problems, constitute a class of problems that are very difficult to solve in practice. Although the exact solution of these problems under certain special cases may be possible, for the general case, there are no known exact solution algorithms. Instead, approximate solution methods have been developed, often restricting the functional form of recourse actions, these are generally referred to as “decision rules“. In this talk, we will present a review of existing decision rule approximations including affine and extended affine decision rules, uncertainty set partitioning schemes and finite-adaptability. We will discuss the reformulations and solution algorithms that result from these approximations. We will point out existing challenges in practical use of these decision rules, and identify current and future research directions. When possible we will emphasize the connections to multi-stage stochastic programming literature.
