---
title: "Idées et pistes pour l'améliorations des méthodes de décompositions en RO"
speaker: François Lamothe
tags: [seminar, seminars_2024_jan_july]
date: 2024-01-22            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: LAAS CNRS
position: Postdoctoral researcher
website: 
tag: lamothe_seminar # unique tag of this project
image: 
imagesize: 
---

Les méthodes de décompositions sont des algorithmes d'optimisation largement utilisés en Recherche Opérationnelle. Malgré leur ancienneté, de nombreuses questions les concernant reste sans résponse certaine. Dans cette présentation, nous ferons une présentation géométrique de deux méthodes de décomposition (Dantzig-Wolfe et Benders) avant de proposer des pistes d'amélioration. Nous aborderons en particulier :
- Le choix des coupes dans les méthodes de coupe type Benders
- La dégénérescence dans la méthode de Dantzig-Wolfe
- L'utilisation et l'interaction d'approximation interne et externe de polyèdres dans la méthode de Dantzig-Wolfe.

