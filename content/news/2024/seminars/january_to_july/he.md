---
title: Maintenance Optimization for Complex Manufacturing Systems Using Stochastic Remaining Useful Life Prognostics
speaker: Junkai He
tags: [seminar, seminars_2024_jan_july]
date: 2024-04-11            # very important
publishdate: 2024-03-15
layout: seminar

# optional parameters
institution: Télécom SudParis
position: Postdoctoral researcher
website: 
tag: he_seminar # unique tag of this project
image: 
imagesize: 
---

We leverage Remaining Useful Life (RUL) prognostic information for preventive maintenance planning in complex manufacturing factories. Such a factory consists of multiple complex systems that use redundant components as backups to ensure system availability. The purpose is to minimize production fluctuations in the factory due to maintenance or system breakdown. To achieve this, we propose Mixed-Integer Linear Programming (MILP) models to minimize the overall production loss. Furthermore, we incorporate random RUL decrease rates according to reliability theory and extend the MILP model using chance-constrained programming. A novel approximation method for dealing with chance constraints is proposed and approximation properties are analyzed. Computational results demonstrate the efficacy of the proposed methods in maintenance planning under different levels of uncertainty.
