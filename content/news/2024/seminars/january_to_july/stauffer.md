---
title: "Horizontal collaboration in forestry - game theory models and algorithms for trading demands"
speaker: Gautier Stauffer
tags: [seminar, seminars_2024_jan_july]
date: 2024-01-11            # very important
publishdate: 2024-01-01
layout: seminar


# optional parameters
institution: HEC Lausanne
position: Professor
website: https://www.linkedin.com/in/gautier-stauffer-b823424

tag: stauffer_seminar # unique tag of this project
image: 
imagesize: 
---


We introduce a new cooperative game theory model that we call production-distribution game to address a major open problem for operations research in forestry, raised by Rönnqvist et al. in 2015, namely, that of modelling and proposing efficient sharing principles for practical collaboration in transportation in this sector. The originality of our model lies in the fact that the value/strength of a player does not only depend on the individual cost or benefit of the goods she owns but also on her market shares (customers demand). We show that our new model is an interesting special case of a market game introduced by Shapley and Shubik in 1969. As such it exhibits the property of having a non-empty core. We prove that we can compute both the nucleolus and the Shapley value efficiently, in a nontrivial, interesting special case. We provide two algorithms to compute the nucleolus: a simple separation algorithm and a fast primal-dual one. We also show that our results can be used to tackle more general versions of the problem and we believe that our contribution paves the way towards solving the challenging open problems herein.

