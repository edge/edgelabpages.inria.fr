---
title: A Column-and-Constraint Generation Algorithm for Bilevel Optimization 
speaker: Henri Lefebvre
tags: [seminar, seminars_2024_jan_july]
date: 2024-03-14            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: University of Trier
position: Postdoctoral researcher
website: https://hlefebvr.github.io/
tag: lefebvre_seminar # unique tag of this project
image: 
imagesize: 
---

Bilevel optimization is a powerful tool for modeling complex hierarchical decisions. In particular, it addresses situations in which decision makers must take into account other agents having an independent utility function while making their decisions. Though challenging on their own, a lot of effort has been conducted by the research community to solve linear, at most convex, bilevel problems, in such a way that it is now conceivable to solve much larger problems than what was conceivable years ago. Unfortunately, most of the existing approaches rely on duality theory and, therefore, cannot be generalized to the nonconvex setting, occurring when all players solve a nonconvex problem. In this talk, we introduce a new algorithm for solving a class of bilevel optimization problems with nonconvex followers and binary leader decisions. This algorithm is based on approximating bilevel problems by generalized two-stage robust problems for which we design a column-and-constraint generation approach. Additionally, we present a stabilization scheme for our method and report some early computational experiments.
