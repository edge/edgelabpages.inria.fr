---
title: Décision séquentielle sous incertitude
speaker: Romain Guillaume
tags: [seminar, seminars_2024_jan_july]
date: 2024-03-28            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: Institut de recherche en informatique de Toulouse
position: Associate professor
website: https://www.irit.fr/~Romain.Guillaume//
tag: guillaume_seminar # unique tag of this project
image: 
imagesize: 

---



L’incertitude est inhérente au problème de décision. Dans de nombreux contextes de conception de système de production : conception d’ateliers de fabrication additive, de chaînes logistiques inverses et de planification minière, de chaines d’assemblages aéronautiques, les décisions ne sont pas toutes prisent simultanément mais de façon séquentielle dépendant de la situation dans laquelle on se trouve et du niveau de connaissance.  La connaissance sur les mondes possibles peut être plus ou moins précise. En fonction de ce degré de connaissance, différentes théories comme celles des probabilités, des fonctions de croyance, des possibilités peuvent être utilisées. L’aspect séquentielle des décisions complexifie l’adoption d’une stratégie optimale. Dans cette exposé, je présenterai dans une première partie les spécificités de la décision séquentielle sous incertitude, puis proposerai des nouveaux critères qui permettent de prendre en compte le comportement fasse a l'incertitude du décideur dans le cas d’incertitude totale et incertitude possibiliste. Une deuxième partie présentera deux théorèmes d’impossibilité permettant de mieux comprendre les difficultés de la décision séquentielle sous incertitude avec des fonctions de croyance. Et le premier critère satisfaisant les propriétés de la décision séquentielle avec fonction de croyance sera présenté.
