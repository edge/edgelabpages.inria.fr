---
title: The Benders-by-batch algorithm to solve two-stage stochastic linear programs
speaker: Boris Detienne
tags: [seminar, detienne, seminars_2024_jan_july]
date: 2024-05-23            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: Université de Bordeaux, Inria-Bordeaux
position: Associate professor
website: https://edge.gitlabpages.inria.fr/team-members/detienne/
tag: detienne_seminar2024 # unique tag of this project
image: 
imagesize: 
---

In this talk, we will introduce a new exact algorithm to solve two-stage stochastic linear programs. Based on the multicut Benders reformulation of such problems, with one subproblem for each scenario, this method relies on a partition of the subproblems into batches. The key idea is to solve at most iterations only a small proportion of the subproblems by detecting as soon as possible that a first-stage candidate solution cannot be proven optimal. We also propose a general framework to stabilize our algorithm, and show its finite convergence and exact behavior. We report an extensive computational study on large-scale instances of stochastic optimization literature that shows the efficiency of the proposed algorithm compared to nine alternative algorithms from the literature. We also obtain significant additional computational time savings using the primal stabilization schemes.
