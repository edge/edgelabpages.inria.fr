---
title: Principe d'estimation et de contrôle pour les processus de décision markoviens en temps continu
speaker: Alexandre Génadot
tags: [seminar, seminars_2024_jan_july]
date: 2024-02-15            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: University of Bordeaux, Inria Bordeaux
position: Associate professor
website: https://www.math.u-bordeaux.fr/~agenadot/
tag: genadot_seminar # unique tag of this project
image: 
imagesize: 
---


