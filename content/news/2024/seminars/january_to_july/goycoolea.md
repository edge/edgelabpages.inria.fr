---
title: Strategic mine planning models under uncertainty
speaker: Marcos Goycoolea
tags: [seminar, seminars_2024_jan_july]
date: 2024-06-13           # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: Universidad Catolica de Chile
position: Professor
website: https://mgoycool.github.io/
tag: goycoolea_seminar # unique tag of this project
image: 
imagesize: 
---


Strategic planning for open pit mines is a challenging problem that is tackled on a regular basis by mines throughout the world. A strategic mine plan is critical for securing investments for a new mining project. It is also of great relevance for operating mines as a mean to plan the timing and scale of future investments, as well as for guiding short term decisions. The strategic mine planning can be framed as a large-scale integer programming problem. This problem has been the object of study in the mining community for more than sixty years.  It is such an important problem that there exist numerous software packages that offer different technologies for solving it. During the last decade major strides have been made in being able to solve this problem to near optimality in practical problems. 

A major shortcoming of existing mine planning technologies is that they ignore the fact that the data used to construct the model is uncertain. Future commodity prices, operational performance of the mine, and the distribution of ore in the mine are in fact unknown and at best can be approximated or modelled with probability distributions.

In this talk we describe our efforts to develop optimization methodologies that consider geological uncertainty (i.e., distribution of the ore in the mine). We show that by adapting the Bienstock-Zuckerberg algorithm it is possible to solve realistic instances of this problem. We test our methodology on multiple mine sites, analyzing our solution with a sampling technique, and performing different types of risk analyses. We find that incorporating uncertainty into the mine planning can significantly change the solutions produced, yielding more reliable mine plans, and higher value strategies.
