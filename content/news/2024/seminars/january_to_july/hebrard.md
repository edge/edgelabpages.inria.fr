---
title: An Efficient Constraint Programming Approach to Preemptive Job Shop Scheduling
speaker: Emmanuel Hébrard
tags: [seminar, seminars_2024_jan_july]
date: 2024-03-21            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: LAAS-CNRS, Université de Toulouse
position: CNRS researcher
website: https://homepages.laas.fr/ehebrard/
tag: hebrard_seminar # unique tag of this project
image: 
imagesize: 
---


Constraint Programming (CP) has been widely, and very successfully, applied to scheduling problems. However, the focus has been on uninterruptible tasks, and preemptive scheduling problems are typically harder for existing constraint solvers. Indeed, one usually needs to represent all potential task interruptions thus introducing many variables and symmetrical or dominated choices.
I will talk about CP approaches to both non-preemptive and preemptive job shop scheduling, and then I will show how methods for the former can be applied with very little change to the latter. The resulting approach does not require an explicit model of task interruptions and as a result significantly outperforms state-of-the-art dedicated approaches in our experimental results.
