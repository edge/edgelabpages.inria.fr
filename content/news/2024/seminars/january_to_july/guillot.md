---
title: Tarification stratégique sur les marchés de l'électricité avec contraintes de pollution
speaker: Gaël Guillot
tags: [seminar, seminars_2024_jan_july]
date: 2024-06-26            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: Inria-Lille
position: Postdoctoral researcher
website:
tag: guillot_seminar2024 # unique tag of this project
image: 
imagesize: 
---

Dans cette présentation, nous exposons un problème bi-niveaux, multi leader- single follower, de tarification sur le marché de l'électricité. Les meneurs correspondent aux sociétés productrices d'énergie qui doivent soumettre une offre à un agent centralisateur (ISO). L'ISO sélectionne les offres et distribue la demande sur le réseaux. Les générateurs sont composés de plusieurs technologies de production, avec différents coûts et quantités de pollution produite. Nous exposerons les particularités de ce problème ainsi que les différents algorithmes qui permettent de trouver un ou plusieurs équilibres de Nash.
