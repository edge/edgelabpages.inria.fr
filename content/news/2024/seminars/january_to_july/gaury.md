---
title: An efficient 2-competitive online algorithm for kit update at MSF Logistique
speaker: Mickael Gaury
tags: [seminar, gaury, seminars_2024_jan_july]
date: 2024-06-20            # very important
publishdate: 2024-01-01
layout: seminar

# optional parameters
institution: Kedge Business School
position: PhD Candidate
website:
tag: gaury_seminar2024 # unique tag of this project
image: 
imagesize: 
---


This paper explores strategic optimization in updating essential medical kits crucial for humanitarian emergencies. Recognizing the perishable nature of medical components, the study emphasizes the need for regular updates involving complex recovery, substitution and disposal processes with the associated costs. The goal is to minimize costs over an unpredictable time horizon. The introduction of the kit-update problem considers both deterministic and adversarial scenarios. Key performance indicators (KPIs), including updating time and destruction costs, are integrated into a comprehensive economic measure, emphasizing a strategic and cost-effective approach.

The paper proposes an innovative online algorithm utilizing available information at each time period, demonstrating its 2-competitivity. Comparative analyses include a stochastic multi-stage approach and four other algorithms representing former and current MSF policies, a greedy improvement of the MSF policy, and the perfect information approach.

Analytics results on various instances show that the online algorithm is competitive in terms of cost with the stochastic formulation, with differences primarily in computation time. This research contributes to a nuanced understanding of the kit-update problem, providing a practical and efficient online algorithmic solution within the realm of humanitarian logistics.
