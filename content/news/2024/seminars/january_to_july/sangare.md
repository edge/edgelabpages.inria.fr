---
title: A distributed scheduling method for demand response in energy communities with distributed generation and storage
speaker: Mariam Sangaré
tags: [seminar, seminars_2024_jan_july]
date: 2024-01-18            # very important
publishdate: 2024-01-01
layout: seminar


# optional parameters
institution: Laboratoire d'Informatique, de Robotique et de Microélectronique de Montpellier (LIRMM)
position: PhD student
tag: sangare_seminar # unique tag of this project
image: 
imagesize: 
---

We present a distributed optimization method for energy communities having distributed renewable generation and storage units. We explain how the resulting optimization problem can be cast as a bi-level optimization problem where the followers solve mixed-integer linear programs. Given the difficulty of these problems, we develop a heuristic where incentives are sent to the followers. Specifically, to maximize the collective-self consumption rate, we exploit the notion of allocation key traditionally used a posteriori for economic gain sharing to build an a priori incentive to Demand response. The proposed method returns high-quality solutions to the optimal centralized real-world benchmark instances constructed over a month with accurate historical data from our demonstrator in South France.

