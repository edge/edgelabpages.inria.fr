# A folder to put the Edge team news

## Organisation

One folder for each year with 4 subfolders
* new_results : to store news related to new results (submitted paper)
* phd_defense : to store news related to phd defenses
* seminars : to store news related to seminars
* resources : to store pictures


## Adding a news

Depending on the type of news you want to add, just copy-paste an existing one and change the information.

## Preview
The preview of each news is by default the title, the subtitle and the date. 
To this preview, you can also add a summary field in the first part of the file. If there is no summary field or if it is empty, then the first 70 characters of the news are displayed.

_Important: Everything after the tag "\<!--more-->" is not displayed_.

## Image
You can associate an image with the collaboration. Use the attribute _imagesize_ to set the size of the image.

If you want to display an image on the page associated with the news (and not just the preview), you can use the following code:
```
<div style="float: right;max-width:600px;padding-right:50px;padding-left:50px;padding-bottom:20px">
<img src="relativePathToTheImage" alt="Test on a prototype car" class="img-title media-object" style="width: 100%;"/></div>
```
or like this:
```
{{< figure src="relativePathToTheImage" class="twext-center" caption="my caption for the image">}}
```
or like this:
```
![](relativePathToTheImage)
```