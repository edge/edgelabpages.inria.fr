---
title: Modeling and solving a stochastic generation and transmission expansion planning problem faced by RTE
speaker: Aurélien Froger
tags: [seminar, seminars_2023_sept_dec, froger ]
date: 2023-09-21            # very important
layout: seminar

# optional parameters
institution:  University of Bordeaux, Inria-Bordeaux
position: Associate professor
website: https://edge.gitlabpages.inria.fr/team-members/froger/
tag: froger_seminar # unique tag of this project
image: 
imagesize: 
---

The presentation is about the following submitted paper:

Xavier Blanchot, François Clautiaux, Aurélien Froger, Manuel Ruiz. (2023). Modeling and solving a stochastic generation and transmission expansion planning problem with a "Loss Of Load Expectation" reliability criterion. [(hal-03957750v2)](https://hal.archives-ouvertes.fr/hal-03957750v2/document) 


