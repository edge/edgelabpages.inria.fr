---
title: A discrete optimization approach to L0-norm problems
speaker: Théo Guyard
tags: [ seminar, seminars_2023_sept_dec ]
date: 2023-10-19            # very important
layout: seminar

# optional parameters
institution:  Inria-Rennes/INSA-Rennes
position: PhD student
website: https://theoguyard.github.io/
tag: guyard_seminar # unique tag of this project
image: 
imagesize: 
---

Problems involving the L0-norm have long been considered too difficult to solve, despite their practical interest. Recently, researchers from the discrete optimization community have shifted their attention towards this class of problems. Using tools traditionally employed in their field, they have unlocked the possibility to handle L0-norm problems in practice. In a first part, this talk will outline the applications of problems involving the L0-norm, their advantages and flaws, and will briefly review the history of the recent research works on this topic. Then, discrete optimization tools that can be used to address this type of problem will be presented. Specifically, we will focus on generic Mixed-Integer Program solvers and specialised Branch-and-Bound algorithms. Existing softwares for practitioners will also be discussed. In a last part, we will provide insights into the ongoing research directions regarding L0-norm problems and about the different researchers and laboratories involved. 
