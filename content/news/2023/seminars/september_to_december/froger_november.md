---
title: Internal workshop on Lagrangian variable fixing techniques
speaker: Aurélien Froger
tags: [seminar, seminars_2023_sept_dec, froger]
date: 2023-11-16            # very important
layout: seminar

# optional parameters
institution:  University of Bordeaux, Inria-Bordeaux
position: Associate professor
website: https://edge.gitlabpages.inria.fr/team-members/froger/
tag: froger_seminar # unique tag of this project
image: 
imagesize: 
---


In this internal workshop, we review the recent literature on Lagrangian variable fixing techniques. 
