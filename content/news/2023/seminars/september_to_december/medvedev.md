---
title: Finite adaptability for robust optimization applied to the glass industry
speaker: Anton Medvedev
tags: [ seminar, seminars_2023_sept_dec ]
date: 2023-11-09            # very important
layout: seminar

# optional parameters
institution:  Saint-Gobain Research/CNAM
position: PhD student
website: https://fr.linkedin.com/in/anton-medvedev-124886110
tag: medvedev_seminar # unique tag of this project
image: 
imagesize: 
---

Finite adaptability is a resolution framework for two-stage robust optimization problems, introduced in 2010 by Bertsimas and Caramanis. We apply this resolution scheme for a glass industry production problem. The latter deals with coated glass production, in which thin layers of materials are deposited on the glass sheets, using a magnetron, to give it visual and thermal properties. The exact production plan is uncertain and the magnetron maintenance decisions have to be taken before the realization of the uncertainty.

Along with the description and formulation of the above stated industrial problem and of its resolution scheme, the presentation will address two theoretical results in the context of finite adaptability. The first one is the convergence of finite adaptability to complete adaptability under some continuity assumption. The second one is a specific setting in which finite adaptability is solved in polynomial time. 
