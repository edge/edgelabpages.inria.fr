---
title: A polyhedral approach to the total matching problem 
speaker: Luca Ferrarini
tags: [ seminar, seminars_2023_sept_dec ]
date: 2023-10-26            # very important
layout: seminar

# optional parameters
institution:  CERMICS-Ecole des Ponts ParisTech
position: Postdoctoral researcher
website: https://www.researchgate.net/profile/Luca-Ferrarini-3
tag: ferrarini_seminar # unique tag of this project
image: 
imagesize: 
---


A total matching of a graph G = (V,E) is a subset of G such that its elements, i.e. vertices and edges, are pairwise not adjacent. In this context, the Total Matching Problem calls for a total matching of maximum size. This problem generalizes both the Stable Set Problem, where we look for a stable set of maximum size and the Matching Problem, where instead we look for a matching of maximum size. In this talk, we present a polyhedral approach to the Total Matching Problem, and hence, we introduce the corresponding polytope, namely the Total Matching Polytope. To this end, we will present several families of nontrivial valid inequalities that are facet-defining for the Total Matching Polytope. In addition, we provide a first linear complete description for trees and complete bipartite graphs. For the latter family, the complete characterization is obtained by projecting a higher-dimension polytope onto the original space. This leads to also an extended formulation of compact size for the Total Matching Polytope of complete bipartite graphs. 

