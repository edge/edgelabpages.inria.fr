---
title: Models and algorithms for configuring and testing prototype cars
speaker: François Clautiaux
tags: [seminar, seminars_2023_sept_dec, clautiaux ]
date: 2023-12-14            # very important
layout: seminar

# optional parameters
institution:  University of Bordeaux, Inria-Bordeaux
position: Professor
website: https://edge.gitlabpages.inria.fr/team-members/clautiaux/
tag: clautiaux_seminar # unique tag of this project
image: 
imagesize: 
---

The presentation is about the following submitted paper:

François Clautiaux, Siham Essodaigui, Alain Nguyen, Ruslan Sadykov, Nawel Younes. (2023). Models and algorithms for configuring and testing prototype cars. [(hal-04185248)](https://hal.archives-ouvertes.fr/hal-04185248/document)
