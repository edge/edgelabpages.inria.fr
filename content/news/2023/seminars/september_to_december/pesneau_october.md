---
title: Maximum chordal sub-graph problem
speaker: Pierre Pesneau
tags: [ seminar, seminars_2023_sept_dec, pesneau ]
date: 2023-10-12            # very important
layout: seminar

# optional parameters
institution:  University of Bordeaux, Inria-Bordeaux
position: Associate professor
website: https://edge.gitlabpages.inria.fr/team-members/pesneau/
tag: pesneau_seminar # unique tag of this project
image: 
imagesize: 
---

The presentation is about a working paper on the maximum chordal sub-graph problem




