---
title: Cutting planes for signomial programming
speaker: Liding Xu
tags: [ seminar, seminars_2023_sept_dec]
date: 2023-09-28           # very important
layout: seminar

# optional parameters
institution: LIX - Ecole Polytechnique
position: PhD student
website: https://lidingxu.github.io/
tag: xu_seminar # unique tag of this project
image: 
imagesize: 
---

Cutting planes are of crucial importance when solving nonconvex nonlinear programs to global optimality, for example using the spatial branch-and-bound algorithms. In this paper, we discuss the generation of cutting planes for signomial programming. Many global optimization algorithms lift signomial programs into an extended formulation such that these algorithms can construct relaxations of the signomial program by outer approximations of the lifted set encoding nonconvex signomial term sets, i.e., hypographs, or epigraphs of signomial terms. We show that any signomial term set can be transformed into the subset of the difference of two concave power functions, from which we derive two kinds of valid linear inequalities. Intersection cuts are constructed using signomial term-free sets which do not contain any point of the signomial term set in their interior. We show that these signomial term-free sets are maximal in the nonnegative orthant, and use them to derive intersection sets. We then convexify a concave power function in the reformulation of the signomial term set, resulting in a convex set containing the signomial term set. This convex outer approximation is constructed in an extended space, and we separate a class of valid linear inequalities by projection from this approximation. We implement the valid inequalities in a global optimization solver and test them on MINLPLib instances. Our results show that both types of valid inequalities provide comparable reductions in running time, number of search nodes, and duality gap.
