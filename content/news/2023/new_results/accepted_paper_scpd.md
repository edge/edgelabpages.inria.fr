---
title: Mathematical models based on decision hypergraphs for designing a storage cabinet
tags: [paper, newresult, 2023, news, marques, clautiaux, froger, adlib, scpd]
comments: false
image: ../../resources/scpd.webp
imagesize: 80
date: 2024-06-06
weight:
layout: paper 
authors:  Luis Marques, François Clautiaux, Aurélien Froger
keywords: Cutting and Packing, Integer linear programming, Temporal knapsack, Arc flow models, Decision hypergraphs
abstract: "We study the problem of designing a cabinet made up of a set of shelves that contain compartments
whose contents slide forward on opening.
Considering a set of items candidate to be stored in the cabinet over a given time horizon, the problem is to design a set of shelves, a set of compartments in each shelf and to select the items to be inserted into the compartments.
The objective is to maximize the sum of the profits of the selected items. We call our problem the \emph{Storage Cabinet Physical Design} (SCPD) problem.
The SCPD problem combines a two-staged two-dimensional knapsack for the design of the shelves and compartments with a set of temporal knapsack problems for the selection and assignment of items to compartments.
We formalize the SCPD problem and formulate it as a maximum cost flow problem in a decision hypergraph with additional linear constraints.
To reduce the size of this model, we break symmetries, generalize graph compression techniques
and exploit dominance rules for precomputing subproblem solutions.
We also present a set of valid inequalities to improve the linear
relaxation of the model.
We empirically show that solving the arc flow model with all our enhancements outperforms solving a compact mixed integer linear programming formulation of the SCPD problem."

---

##### **Manuscript**

The manuscript is published in <i>European Journal of Operational Research</i> (DOI: <a href="https://doi.org/10.1016/j.ejor.2024.09.022">10.1016/j.ejor.2024.09.022</a>) and is open access.
<br/>
{{% center %}}
<a href="https://doi.org/10.1016/j.ejor.2024.09.022" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>


