---
title: VRPSolverEasy - a Python library for the exact solution of a rich vehicle routing problem
tags: [paper, newresult, 2023, news, errami, sadykov]
comments: false
image: ../../resources/VRPSolverEasy.webp
imagesize: 30
date: 2023-11-11
weight:
layout: paper 
authors:  Najib Errami, Eduardo Queiroga, Ruslan Sadykov, Eduardo Uchoa
abstract: "The optimization community has made significant progress in solving Vehicle Routing Problems (VRPs) to optimality using sophisticated Branch-Cut-and-Price (BCP) algorithms. VRPSolver is a BCP algorithm with excellent performance in many VRP variants. However, its complex underlying mathematical model makes it hardly accessible to routing practitioners. To address this, VRPSolverEasy provides a Python interface to VRPSolver that does not require any knowledge of Mixed Integer Programming modeling. Instead, routing problems are defined in terms of familiar elements such as depots, customers, links, and vehicle types. VRPSolverEasy can handle several popular VRP variants and arbitrary combinations of them."
---



##### **Manuscript**

The manuscript is published in <i>INFORMS Journal On Computing</i> (DOI: <a href="https://doi.org/10.1287/ijoc.2023.0103">10.1287/ijoc.2023.0103</a>).
<br/>
{{% center %}}
<a href="https://hal.science/hal-04057985v2/document" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>

##### **Code**
The code is available on <a href="https://github.com/inria-UFF/VRPSolverEasy">GitHub</a>.



