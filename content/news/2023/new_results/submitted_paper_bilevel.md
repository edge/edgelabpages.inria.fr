---
title: Modeling and solving a stochastic generation and transmission expansion planning problem with a “Loss Of Load Expectation” reliability criterion
tags: [paper, newresult, 2023, news, blanchot, clautiaux, froger, rte]
comments: false
image: ../../resources/bilevelGTEPP.webp
imagesize: 100
date: 2023-07-21
weight : 
layout: paper
authors:  Xavier Blanchot, François Clautiaux, Aurélien Froger, Manuel Ruiz
keywords:  Stochastic optimization, Mixed integer linear programming, Bilevel programming, Benders decomposition, Matheuristic, Transmission Expansion planning
abstract: "In this paper, we study how a regulatory constraint limiting a measure of unserved demand, called Loss Of Load Expectation (LOLE), can be incorporated into a strategic version of a stochastic generation and transmission expansion planning problem. This problem is tackled by the French Transmission System Operator RTE for producing prospective reports on the evolution of the electricity network. We show that a direct inclusion of the constraint into the extensive form of the two-stage stochastic problem leads to a formulation that violates the time-consistency principle. To obtain a valid model, we use bilevel programming and introduce a formulation of the problem in which the leader and follower have the same objective function. To solve this formulation, we propose a matheuristic that embeds a Benders decomposition algorithm in a binary search on the total investment cost. 

</br>
We performed computational experiments to study the practical difficulty of the problem and validate the proposed solution method. Our experiments show that solving the single-level reformulation of the problem obtained using the KKT complementary conditions is intractable in practice, even for small size instances, and that a simple heuristic procedure is not sufficient to compute feasible solutions for all test cases. This is not the case for our matheuristic, which finds a feasible solutions for all instances of our test bed."
 
---


##### **Manuscript**

The manuscript can be found in HAL database.
<br/>
{{% center %}}
<a href="https://hal.science/hal-03957750v2/document" role="button" class="btn btn-primary">Access the manuscript</a>
{{%/ center %}}
<br/>



