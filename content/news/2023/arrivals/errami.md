---
title: "Najib Errami joins EDGE"
tags: [ news, Arrival, adlib, errami]
date: 2023-12-01            # very important
subtitle: Engineer on the ADLib project

# optional parameters
tag: # unique tag of this project
image: ../../../../team-members/resources/najib.webp
imagesize: 20
---

We are happy to announce that [Najib Errami]({{<  ref "team-members/errami.md"  >}}) has joined our team as an engineer. Najib will work on the [project ADLib]({{<  ref "/projects/adlib.md"  >}}) which aims to create a library for Aggregation/Disaggregation of large network flow models.
