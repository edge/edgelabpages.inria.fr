---
title: Anchor-robust project scheduling with non-availability periods
speaker: Luca Brunod
tags: [seminar, seminars_2025_jan_july]
date: 2025-02-20           # very important
publishdate: 2025-01-01
layout: seminar

# optional parameters
institution: EDF R&D, Sorbonne Université - LIP6
position: PhD student
website: 
tag: brunod_seminar # unique tag of this project
image: 
imagesize: 
---


When planning large projects, it is convenient to rely on a baseline schedule, which gives a preview of logistical requirements. However, project data is often uncertain and jobs may be interrupted due to fortuitous causes of unavailability. Disruptions occurring between the planning and the execution of the project may cause the baseline schedule to become unfeasible. Part of the project must then be rescheduled, possibly entailing large costs.

Anchor robustness is a two-stage robust approach that aims at computing a baseline schedule in which a subset of so-called anchored jobs have their starting times guaranteed against any disruption in a given uncertainty set. Anchor-robust scheduling, that was previously studied for processing time uncertainty, is extended to the case of uncertain unavailability periods.
