---
title: Some combinatorial and stochastic optimization problems that arise when auditing biases in online algorithms
speaker: Benoit Rottembourg
tags: [seminar, seminars_2025_jan_july]
date: 2025-01-30           # very important
publishdate: 2025-01-01
layout: seminar

# optional parameters
institution: Centre Inria de Paris
position: Researcher
website: https://www.linkedin.com/in/rottembourg/
tag: rottembourg_seminar # unique tag of this project
image: 
imagesize: 
---


Marketplaces will account for more than 62% of global e-commerce sales by 2023, with their turnover increasing 4-fold in less than 10 years. At the same time, e-commerce continues to grow. By 2023, it will be worth 159.9 billion euros in France, accounting for 10% of retail sales. The recommendation algorithms - the famous buyboxes - of these marketplaces guide our choices. But what do we know about their loyalty? How do we know if they are deceiving us, if they are biased or if they are just maximizing the platform's turnover?

We sought to show how to audit the fairness of these algorithms, seen as black boxes, by studying the case of Amazon, on several thousand products in France. We will show that the search for a prejudicial bias (for the consumer or for the platform's competitors) raises the question of finding a context of use in which the bias is statistically significant.

We will illustrate the combinatorial optimization problem that arises when the bias is ‘regional’ (neither global nor local to a decision) and when the sensitive variable (describing the discriminated population) is continuous and non-binary.

Finally, and more generally, we will describe the issues that arise when the query budget for the black-box algorithm is limited and when several auditors cooperate to detect bias more effectively.
