---
title: Cooperative game theory and cost allocation in energy communities
speaker: Mariam Sangaré
tags: [seminar, seminars_2025_jan_july, sangare]
date: 2025-02-20           # very important
publishdate: 2025-01-01
layout: seminar

# optional parameters
institution: Centre Inria de l'université de Bordeaux
position: Postdoctoral researcher
website: 
tag: sangare_seminar # unique tag of this project
image: 
imagesize: 
---


An energy community (EC) is a legal entity involving prosumers and consumers who produce, consume, and exchange energy. The members of these communities can cooperate to maximize the community’s social welfare. In practice, this naturally raises the question of cost sharing in the community, as the members may have different contributions to social welfare. In this presentation, we empirically highlight the benefits of cooperation for the community and the individual members. Then, we present some cost-sharing mechanisms that guarantee fairness and the stability of the grand coalition composed of all prosumers and consumers. Finally, we present some results on instances built with real-world data from our partner Sween’s demonstrator, Smart Lou Quila, in South France.
