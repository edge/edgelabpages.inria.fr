---
title:     New perspectives on invexity and its algorithmic applications
speaker: Ksenia Bestuzheva
tags: [seminar, seminars_2025_jan_july]
date: 2025-03-11           # very important
publishdate: 2025-01-01
layout: seminar

# optional parameters
institution: Zuse-Institut Berlin
position: Researcher
website: https://kbestuzheva.github.io/
tag: bestuzheva_seminar # unique tag of this project
image: 
imagesize: 
---


One of the key properties of convex problems is that every stationary point is a global optimum, and nonlinear programming algorithms that converge to local optima are thus guaranteed to find the global optimum. However, some nonconvex problems possess the same property. This observation has motivated research into generalizations of convexity. This talk proposes a new generalization which we refer to as optima-invexity: the property that only one connected set of optimal solutions exists. We state conditions for optima-invexity of unconstrained problems and discuss structures that are promising for practical use, and outline algorithmic applications of these structures.
