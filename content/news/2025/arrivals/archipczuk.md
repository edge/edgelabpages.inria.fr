---
title: "Paul Archipczuk joins EDGE"
tags: [ news, Arrival, paul-archipczuk]
date: 2025-02-01            # very important
subtitle: Engineer on the Grip4all project

# optional parameters
tag: # unique tag of this project
image: ../../../../team-members/resources/paul-archipczuk.webp
imagesize: 20
---

We are happy to announce that [Paul Archipczuk]({{<  ref "team-members/paul-archipczuk.md"  >}}) has joined our team as an engineer. Paul will work on the [project Grip4all]({{<  ref "/projects/grip4all.md"  >}}) about robotic palletizing of heterogeneous products without prior scheduling.
