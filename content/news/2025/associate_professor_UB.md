---
title: "A permanent position of associate professor is available within our team"
tags: [ news]
date: 2025-03-04            # very important
subtitle: University of Bordeaux - Inria EDGE 

# optional parameters
tag: # unique tag of this project
image: 
imagesize: 20
---

We are pleased to announce that a permanent associate professor position (maître de conférences) at the University of Bordeaux is available for the 2025 national recruitment campaign. 
The successful candidate will join the Edge team. 

<!--more-->

A summary of the job description is given below in French. Please do not hesitate to contact the team if you are interested in applying.

____________________________

**Profil pédagogique :** Recherche opérationnelle, optimisation mathématique

**Affectation pédagogique :** Collège Sciences et technologies - UF Mathématiques et interactions

**Filières de formation concernées :** Licence Mathématiques, Licence Miashs, Master MAS

**Objectifs pédagogiques et besoin d'encadrement :**

Les enseignements correspondant à ce poste se dérouleront au sein des filières de l’Unité de Formation Mathématiques et Interactions. La personne recrutée interviendra dans les différentes disciplines mathématiques autour de l’optimisation et de la recherche opérationnelle. En particulier, elle participera aux enseignements dans le parcours Recherche Opérationnelle et Aide à la Décision (ROAD) du master mention Mathématiques appliquées, statistiques, en licence de mathématiques, et aidera au fonctionnement du Cursus Master Ingénierie OPTIM. Ces enseignements prendront la forme de cours magistraux, de travaux dirigés, de travaux pratiques, et d’encadrement de projets et de stages.

On s’attend à ce que la personne recrutée ait des compétences fortes en optimisation numérique (algorithmes d’optimisation, pratique des solveurs d’optimisation) et en modélisation. Une expérience préalable dans des projets avec applications pratiques serait aussi très appréciée. En licence, la personne participera aux enseignements généraux de mathématiques, ainsi que des enseignements spécialisés en optimisation numérique, programmation, mathématiques discrètes, en présentiel et en suivi de projets.

Au niveau master, la personne recrutée sera aussi amenée à participer au suivi d’étudiants en apprentissage dans le parcours ROAD. En CMI OPTIM, on attendra d’elle qu’elle participe à diverses activités liées au suivi personnalisé d’étudiants, par exemple le suivi des stages à l’étranger, des projets, ou des enseignements spécifiques à la formation. La personne recrutée sera encouragée à participer au programme Numerics, en particulier la semaine thématique à laquelle l’équipe participe. Elle pourra aussi participer au montage d’activités de vulgarisation ou d’opérations liées au tiers-lieu IA/data créé dans le cadre du projet CAP IA.


____________________________

**Profil Recherche :** Recherche opérationnelle, Optimisation mathématique

**Unité de recherche d’accueil :** Département Sciences de l'Ingénierie et du Numérique - Institut de Mathématiques de Bordeaux (IMB)

**Description du projet de recherche :**

L’équipe OptimAl a développé une expertise forte dans les méthodes de résolution de problèmes d’optimisation de grande taille, et en particulier dans les méthodes de décomposition de grands programmes linéaires en nombres entiers déterministes ou sous données incertaines. Dans les années qui viennent de s’écouler, des progrès très importants ont été réalisés dans la résolution de problèmes combinatoires où interviennent des aspects traditionnellement difficilement traités : des incertitudes sur les données, ou des non linéarités. Ces progrès proviennent à la fois de résultats mathématiques, d’outils méthodologiques émergents, et de la nécessité de traiter ces problèmes issus d’applications pratiques (dans l’énergie et le transport, par exemple).
Pour accompagner son développement, l’équipe souhaite se renforcer dans un ou plusieurs domaines parmi les suivants : programmation en nombres entiers, méthodes de décomposition, reformulations, matheuristiques, optimisation non linéaire, optimisation robuste, optimisation stochastique, théorie des jeux, analyse polyédrale, lien entre apprentissage machine et optimisation.
La personne candidate sera attachée à l'IMB et sera invitée à intégrer l’équipe-projet commune Inria/Université de Bordeaux EDGE.

**Profil recherché :**
Les candidatures attendues comportent un profil en recherche opérationnelle. L'expertise théorique de la personne candidate devra s'accompagner d'une compétence forte en optimisation numérique et résolution d'applications pratiques, ou une connaissance des domaines d'application prioritaires de l’équipe (énergie, transport, réseaux) ou du site bordelais (santé, écologie).

**Impact scientifique attendu :**
La personne recrutée participera aux nombreux projets de l’équipe, que ce soit des projets purement académiques, ou en partenariat avec des entreprises ou des organisations. Elle sera encouragée à poursuivre sa recherche et s’intégrera à l’activité de l’équipe dans ses aspects théoriques, pratiques et/ou logiciels
