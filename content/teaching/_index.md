---
title: Teaching
---

## Main teaching activities

We teach at the [**University of Bordeaux**](https://www.u-bordeaux.fr/en) in the maths department ([**UFMI**](https://math-interactions.u-bordeaux.fr/)). 

- [**Master degree in Applied Mathematics, Operations Research curriculum** (ROAD)](https://uf-mi.u-bordeaux.fr/road/)
- [**Master of Engineering in Optimization: CMI OPTIM**](https://uf-mi.u-bordeaux.fr/optim/)
- [**Graduate research program: Numerics**](https://doctorat.u-bordeaux.fr/en/before-phd/graduate-programs/numerics)
- [**Bachelor of Mathematics**](https://math-interactions.u-bordeaux.fr/nos-formations/licences/licence-mathematiques)

We also teach at [**Bordeaux INP**](https://www.bordeaux-inp.fr/en).

## Outreach

- We are part of the [Cap IA program](https://www.u-bordeaux.fr/actualites/ami-cma-5-projets-laureats-pour-luniversite-de-bordeaux) at the University of Bordeaux
- We are part of the [Chiche!](https://chiche-snt.fr/qui-sommes-nous/) program
- We regularly organize activities for the science fair in Bordeaux 
- The team was involved in the special issue of Tangente on "Operations Research"

<img src="resources/tangente.webp" alt="Tangente" width="160px">
