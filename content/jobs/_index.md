---
title: Our job offers
subtitle: List of available job offers at EDGE
layout : list_tag_withpast # layout which enables past/ongoing separation

past_title : Past job offers # the title of past topics

list_tag : [job] # the tag used for all job postings
---
{{% center %}}
We are always in search for new people, even if there is no job opportunity on-line.<br>
{{%/ center %}}

<!--x 
Everything before these tags will be displayed before the list of job offers and everyting below ward will be displayed after the list
x-->


