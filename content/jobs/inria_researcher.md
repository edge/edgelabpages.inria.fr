---
title: Researcher position
tags: [job]
subtitle: Inria  # optional

# optional parameters
tag: researcher # unique tag of this project
date:        # optional - for better sorting of posts
weight: 1

# job posting specific and optional
type: Researcher position
duration: # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: true  # if false it will the offer will be displayed in 'past jobs'

summary: There are **open research positions every year** at Inria. <br/> If you are interested in team EDGE we encourage you to contact us directly by email&#58; francois.clautiaux@inria.fr 
---


There are **open research positions every year** at Inria. These are **tenured and full-time research** positions with no mandatory teaching responsibilities.

We are looking for candidates with a PhD in mathematical programming, combinatorial optimization or related fields. There is no age restriction to apply.
Candidates with 2 to 3 years of postdoctoral research experience have the highest chance to be selected, but younger (or older) candidates are welcome. 

https://www.inria.fr/en/researchers-normal-class-crcn

If you are interested in team EDGE, we encourage you to contact us directly by email: francois.clautiaux@inria.fr 
