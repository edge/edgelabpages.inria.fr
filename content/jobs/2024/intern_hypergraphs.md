---
title: Internship position
tags: [job, clautiaux]
subtitle: Valid inequalities in hypergraph flows  # optional

# optional parameters
tag: 2024_intern_hypergraph # unique tag of this project
date:     # optional - for better sorting of posts
weight: 31

# job posting specific and optional
type: Internship
duration: 6 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: false  # if false it will the offer will be displayed in 'past jobs'

summary: A six-month internship at the Inria center of the University of Bordeaux (EDGE team) is open.
---

**Context:** The work is realized in the context of our activities on extended mixed-integer formulations.

**Keywords:** Mixed integer linear programming, Dynamic Programming, Hypergraphs, Branch-And-Cut

**Starting date:** From 2024/02 to 2024/09

**Duration:** 6 months

The objective of the internship is to design and implement branch-and-cut techniques to solve hard combinatorial problems that can be modelled as min-cost flow problems in hypergraphs. 
These algorithms will be embedded into a general framework for flow-related problems developed in the team. 

**Supervision:**
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)


**Skills**
* integer linear programming (branch-and-bound, valid inequalities)
* Network flow algorithms 
* Object oriented programming, C++


**Environment:**

The work will primarily be conducted in the Inria project-team Edge, in the building of the Mathematics Institute of Bordeaux located at Talence.


