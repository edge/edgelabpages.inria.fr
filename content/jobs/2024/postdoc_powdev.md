---
title: Postdoctoral researcher
tags: [job, clautiaux,powdev]
subtitle: PowDev # optional

# optional parameters
tag: 2024_postdoc_powdev # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2

# job posting specific and optional
type: Postdoc position
duration: 24 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: true  # if false it will the offer will be displayed in 'past jobs'

summary: A two year postdoc position in our team is open. 
---


**The project**

This postdoc position is funded by PowDev project (PEPR TASE). The main objective of this project is to evaluate and optimize the resilience of power systems in the context of a massive insertion of renewable energies. The project aims to elaborate a comprehensive and integrated set of decision support tools by considering extreme events in present and future climates, the complexity of the power grid, and socio-economic scenarios.

Particularly, the PowerDev project seeks to achieve the following tasks:
- Simulate, characterize, and analyze the scenario of a blackout in the case of a power system with massive renewable energy (PSMRE), including extreme weather events arising from climate change under present and future projections, realistic electrotech-nical and field knowledge, inherent grid complexity including interactions with other critical infrastructures, economics, and societal impact models.
- Propose a quantitative and systemic framework to optimize the
  resilience of a PSMRE. Specifically focused on addressing possible
  remedial solutions to apply in the design and operational phases to
  maximize the system's resilience against major blackouts.

**Mission**

The postdoctoral researcher hired will work in the mathematical optimization team EDGE in Bordeaux, in collaboration with the UMI SOURCE economic department of Université Paris Saclay, UVSQ, IRD.
The recruited person will help produce models and algorithms for various variants of blackout countermeasure optimization, where the objective function involves social welfare metrics (such as equity and fairness). 
The tasks will be the following: a literature review on power system optimization and socio-economic objective functions, proposing models based on the literature for variants of the problem with various approximations for the grid dynamics and different objective functions, and assessing the quality of the models using benchmarks produced by other project partners.

  

**The investigators**
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- Stéphane Goutte (UMI Source, UVSQ, IRD)
