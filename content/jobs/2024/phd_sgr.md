---
title: PhD student
tags: [job, clautiaux,arslan]
subtitle: CIFRE at Saint Gobain Recherche # optional

# optional parameters
tag: 2024_phd_sgr # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2

# job posting specific and optional
type: PhD position
duration: 36 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: false  # if false it will the offer will be displayed in 'past jobs'

summary: A three-year PhD position at Saint-Gobain Recherche with the Inria center of the University of Bordeaux (EDGE team) is open.
---

The investigators:
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- [Ayse Nur Arslan]({{< ref "/team-members/arslan" >}}) (Edge)
