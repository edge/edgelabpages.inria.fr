---
title: PhD student
tags: [job, detienne, powdev]
subtitle: PowDev # optional

# optional parameters
tag: 2024_phd_powdev # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2

# job posting specific and optional
type: PhD position
duration: 36 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: true  # if false it will the offer will be displayed in 'past jobs'

summary: A three-year PhD position at the Inria center of the University of Bordeaux (EDGE team) is open.
---

**The project**

Strategic Power Systems Development for the Future (PowerDev), funded by PEPR TASE, studies optimization methods and reliability/resilience engineering applied to large-scale electrical power systems. The project is led by CentraleSupélec at the University of Paris Saclay and is composed of a consortium of higher education institutions across France (CentraleSupelec, UVSQ, University Grenoble Alpes), as well as research organizations (Inria, CNRS).

**Research topic and objectives**

Modern power systems are expected to become increasingly complex to design and operate due to the growing number of renewable energy sources (RES). Renewable energy generation is, by nature, intermittent and introduces an amount of uncertainty that severely affects the physical responses of the power system, particularly in terms of voltage control and frequency regulation. Moreover, RES integration within the power system requires introducing many new power electronic devices, which add to the system’s complexity and increase its possible failure modes. Combined with unexpected initiating events, these two main features can lead to cascading failure risks, triggering disastrous consequences to the power grid and, most notably, large-scale blackouts. The economic and societal implications to the impacted regions are usually massive, with financial loss measured in the tens of billions of dollars. The main objective of this project is to evaluate and optimize the resilience of power systems in the context of a massive insertion of renewable energies. The project aims to elaborate a comprehensive and integrated set of decision support tools by considering extreme events in present and future climates, the complexity of the power grid, and socio-economic scenarios.

<img src="powdev.webp" class="img-title media-object" style="width: 100%;"/>

Particularly, the PowerDev project seeks to achieve the following tasks:

    Simulate, characterize, and analyze the scenario of a blackout in the case of a power system with massive renewable energy (PSMRE), including extreme weather events arising from climate change under present and future projections, realistic electrotechnical and field knowledge, inherent grid complexity including interactions with other critical infrastructures, economics, and societal impact models.
    Propose a quantitative and systemic framework to optimize the resilience of a PSMRE. Specifically focused on addressing possible remedial solutions in the design and operational phases to maximize the system’s resilience against major blackouts.

**The investigator**
- [Boris Detienne]({{< ref "/team-members/detienne" >}}) (Edge)
