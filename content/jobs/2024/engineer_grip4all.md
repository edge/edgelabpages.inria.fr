---
title: Engineer
tags: [job, clautiaux, grip4all]
subtitle: Grip4all # optional

# optional parameters
tag: 2024_eng_grip4all # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2
tag : grip4all

# job posting specific and optional
type: Engineer position
duration: 24 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: false  # if false it will the offer will be displayed in 'past jobs'

summary: A two year engineer position dedicated to 3D robot pallet loading is open within project Gril4all. 
---

This engineer position is funded by a competitive project in robotics and logistics.

The Grip4All project aims to make the industry more competitive by developing a new palletising cell adapted to the severe constraints imposed on the logistics flow when handling mixed products (of varying dimensions and weight) and arranging them on a pallet, without having to sort them manually upstream. This new type of palletising meets a strong demand from several sectors, notably mass distribution and the food industry. It meets the demand for handling heterogeneous products without imposing constraints on their packaging, which significantly improves productivity and eliminates tedious human tasks. No such solution currently exists on the market. The flexible robotics issues addressed will be transposable to other logistics processes in the future factories.
  
Our objective is to develop algorithms for the optimised design of palletising plans. Two main levers are considered: the order of arrival of items and the design of pallet loading plans. Given a list of items with physical properties and assigned to orders and a location, and a list of pallets, we aim to calculate an order of arrival for each item and to place the items on the pallets by considering two criteria: the number of pallets used and the pallet balance score. Several technical issues need to be considered. Firstly, the efficiency of the system will be highly dependent on the order of arrival of the items. The order is constrained by the general organisation of the warehouse: several configurations will be evaluated. Secondly, the pallet construction algorithms will be adapted to the gripping system chosen. We will implement an approach based on the team's expertise in several combinatorial optimisation tools: mathematical programming (MIP), heuristics, constraint programming (PPC) and the combination of optimisation algorithms with machine learning tools. 
  
The recruited engineer will help design models and algorithms for various variants of 3D packing and pallet-loading problems with specific constraints and evaluate them against data provided by the partner. 
  
There is a possibility to be hired as a PhD student after the end of the contract. It is also open to candidates with a PhD.  


The investigators
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
