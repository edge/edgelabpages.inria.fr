---
title: Two internship positions
tags: [job, detienne, arslan]
subtitle: Robust optimization with decision-dependent uncertainty  # optional

# optional parameters
tag: 2024_intern_ddrop # unique tag of this project
date:     # optional - for better sorting of posts
weight: 31

# job posting specific and optional
type: Internship
duration: Approx. 6 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: true  # if false it will the offer will be displayed in 'past jobs'

summary: Two six-month internship at the Inria center of the University of Bordeaux (EDGE team) are open.
---

**Context:** The work is realized in the context of the projects [ANR DDROP]({{< ref "/projects/ddrop/" >}}) and [PEPR PowDev]({{< ref "/projects/powdev/" >}}) (Edge).

**Keywords:** robust optimization, mathematical programming, network flows

**Starting date:** Approx. from 2024/02 to 2024/09

**Duration:** 6 months

The objective of the internship is to study robust optimization problems with decision-dependent uncertainty.

The full description of the offer is [here](Sujets_de_Stage_2024_2025.pdf).

**Supervision:**
- [Boris Detienne]({{< ref "/team-members/detienne" >}}) (Edge)
- [Ayse Nur Arslan]({{< ref "/team-members/arslan" >}}) (Edge)


**Skills**
* mixed-integer linear programming
* C++, Python or Julia
* Knowledge in decomposition methods in MILP and optimization under uncertainty approaches is appreciated


**Environment:**

The work will primarily be conducted in the Inria project-team Edge, in the building of the Mathematics Institute of Bordeaux located at Talence.


