---
title: Postdoctoral researcher
tags: [job, arslan]
subtitle: Postdoctoral research position within the project DROI # optional

# optional parameters
tag: 2024_postdoc_droi # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2

# job posting specific and optional
type: Postdoctoral research position
duration: 12 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: true  # if false it will the offer will be displayed in 'past jobs'

summary: A one year postdoctoral research position is open within the project DROI. 
---

A one-year postdoctoral research position is open within the ANR (French National Research Agency) project [DROI](/projects/droi/). Project DROI studies exact and approximate solution approaches as well as dual bounds for adjustable robust optimization problems. 

We are searching for highly motivated candidates with a PhD thesis in mathematical programming approaches in optimization under uncertainty. Knowledge and experience in decomposition methods, numerical implementation and robust optimization are a plus. 

**Expected start date:** no later than October 2025

**Employer:** Inria-Bordeaux

**Expected salary:** 2250 euros (plus benefits)

**Contact:** [Ayse N. Arslan]({{< ref "/team-members/arslan" >}}) (ayse-nur.arslan@inria.fr)
