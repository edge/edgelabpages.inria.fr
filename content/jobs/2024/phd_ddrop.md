---
title: PhD student
tags: [job, detienne, arslan, ddrop]
subtitle: PhD thesis within the ANR project DDROP # optional

# optional parameters
tag: 2024_phd_ddrop # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2

# job posting specific and optional
type: PhD position
duration: 36 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: true  # if false it will the offer will be displayed in 'past jobs'

summary: A three-year PhD position is open within the project DDROP. 
---

A three-year PhD position is open within the ANR (French National Research Agency) project [DDROP](/projects/ddrop/). This project considers robust optimization problems with decision-dependent uncertainty structures. The PhD thesis will study general solution methods for these problems as well as their application to the approximate solution of adjustable robust optimization problems. 

We are searching for candidates with a master's degree in operations research or related fields. Knowledge in advanced topics such as decomposition methods and optimization under uncertainty approaches is a plus. 

A 6-month intership period can precede the PhD thesis.

**Expected start date:** October, 2025 (February/March 2025 for the internship)

**Employer:** Inria-Bordeaux

**Expected salary:** 1,700 euros (plus benefits)

**Contacts:** ayse-nur.arslan@inria.fr, boris.detienne@math.u-bordeaux.fr

**Supervision:**
- [Boris Detienne]({{< ref "/team-members/detienne" >}}) (Edge)
- [Ayse N. Arslan]({{< ref "/team-members/arslan" >}}) (Edge)
