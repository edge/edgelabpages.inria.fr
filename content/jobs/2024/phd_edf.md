---
title: PhD student
tags: [job, clautiaux,froger]
subtitle: CIFRE at EDF # optional

# optional parameters
tag: 2024_phd_edf # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2

# job posting specific and optional
type: PhD position
duration: 36 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: false  # if false it will the offer will be displayed in 'past jobs'

summary: A three-year PhD position at EDF with the Inria center of the University of Bordeaux (EDGE team) is open.
---

The topic of the thesis is related to the unit commitment problem, and
decomposition methods. 



The investigators:
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- [Aurélien Froger]({{< ref "/team-members/froger" >}}) (Edge)
