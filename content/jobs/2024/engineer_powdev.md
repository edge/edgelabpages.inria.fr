---
title: Research engineer
tags: [job, clautiaux,detienne,powdev]
subtitle: PowDev # optional

# optional parameters
tag: 2024_eng_powdev # unique tag of this project
date:     # optional - for better sorting of posts
weight: 2

# job posting specific and optional
type: Engineer position
duration: 12 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: true  # if false it will the offer will be displayed in 'past jobs'

summary: A one year engineer position in our team is open. 
---

This engineer position is funded by PowDev project (PEPR TASE). The main objective of this project is to evaluate and optimize the resilience of power systems in the context of a massive insertion of renewable energies. The project aims to elaborate a comprehensive and integrated set of decision support tools by considering extreme events in present and future climates, the complexity of the power grid, and socio-economic scenarios.

Particularly, the PowerDev project seeks to achieve the following tasks:
- Simulate, characterize, and analyze the scenario of a blackout in the case of a power system with massive renewable energy (PSMRE), including extreme weather events arising from climate change under present and future projections, realistic electrotech-nical and field knowledge, inherent grid complexity including interactions with other critical infrastructures, economics, and societal impact models.
- Propose a quantitative and systemic framework to optimize the
  resilience of a PSMRE. Specifically focused on addressing possible
  remedial solutions to apply in the design and operational phases to
  maximize the system's resilience against major blackouts.
  
The recruited engineer will help producing models and algorithms for
various variants of complex systems involving cascading effects, and evaluate
them against real-life and/or synthetic data. 
  
There is a possibility to be hired as a PhD student after the end of
the one year contract. 


The investigators:
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- [Boris Detienne]({{< ref "/team-members/detienne" >}}) (Edge)
