# A folder to put the Edge team job offers

## Organisation

One folder to store the job offers

## Adding a job offer

Just copy-paste an existing one and change the information.

## Preview

The preview of each job offer is by default the title, the subtitle and the date. 
To this preview, you can also add a summary field in the first part of the file. If there is no summary field or if it is empty, then the first 70 characters of the job offer are displayed.

_Important: Everything after the tag "\<!--more-->" is not displayed_.

## Tags

You should give a unique value to the attribute _tag_.

For better indexing, you should add to the attribute _tags_ the list of tags of people associated with the job offer.

## Past job offers

The attribute _active_ should be set to _false_ to put the job offers to the past offers category.