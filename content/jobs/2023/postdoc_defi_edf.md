---
title: Postdoctoral position
tags: [job, arslan, detienne, froger]
subtitle: Challenge Inria-EDF - Decomposition algorithms for solving generation expansion planning problems  # optional

# optional parameters
tag: 2024_postdoc_defi # unique tag of this project
date:     # optional - for better sorting of posts
weight: 11

# job posting specific and optional
type: Post-Doctoral position
duration: 24 months # optional 
#info_url: https://recrutement.inria.fr/public/classic/en/offres/2023-06293 # optional link to another file/website
active: false  # if false it will the offer will be displayed in 'past jobs'

summary: A two-year postdoctoral position is open at the Inria center of the University of Bordeaux (EDGE team) with a collaboration with EDF. 
---
**Context:** [Description of our collaboration with EDF]({{< ref "/collaborations/defi_inria_edf" >}}).

**Keywords:** Mixed integer linear programming, Benders decomposition, Lagrangian duality, Dynamic Programming

**Starting date:** From 2023/11 to 2024/03

**Duration:** 24 months

**Supervision:**
- Inria: [Ayse Arslan]({{< ref "/team-members/arslan" >}}), [Boris Detienne]({{< ref "/team-members/detienne" >}}), [Aurélien Froger]({{< ref "/team-members/froger" >}}) (Edge), Bernard Fortz, Frédéric Semet (Inocs)
- EDF: Cécile Rottner, Rodolphe Griset (R&D/OSIRIS)


**Work description:**

 The first goal of this study will be to gather the state-of-the-art of the latest algorithmic
developments for solving two- and multi-stage stochastic problems with continuous and integer recourse.
For two-stage problems, we will be interested in recent advances in methods based on Benders decom-
position (Rahmaniani et al., 2017), especially stabilization, strategies aiming to balance the information
between the master and subproblems, batching of subproblems (Blanchot et al., 2023), and cut strength-
ening (Chen and Luedtke, 2022). We will also study algorithms based on Lagrangian dual decomposition
such as progressive hedging or bundle methods for which the application of strategies like branch-and-
bound allow to handle the integer recourse case (Atakan and Sen, 2018; Kim and Dandurand, 2022). For
multi-stage problems, we will be interested in stochastic dual dynamic integer programming (SDDiP) (Zou
et al., 2019) which has been applied to investment problems (Lara et al., 2020) sharing similarities with
the problem studied by EDF.

Using publicly available open-source implementations of the aforementioned techniques (for instance (Kim
and Zavala, 2018)) and implementations that will be developed by the postdoctoral researcher, a numerical
study will be conducted in order to quantify the loss of optimality due to approximations (multi/two-stage
and continuous/integer recourse) on smaller-scale problems having the same structure as the application
at hand. This part of the study will aim to identify the limitations of the existing approaches as well as
determine the most relevant approximation of the target problem.
We will then work on trying to solve the scientific roadblocks determined in the first phase for the most
relevant version of the problem. Improving existing methodologies based on the special structure of the
problem at hand, or developing heuristic version of existing approaches are possible avenues that might
be explored.

**Profile:**

 PhD in Operations research or Mathematical optimization with the following required skills:
- Mixed-integer linear programming
- Significant experience with programming languages (C++ preferred)
Skills/knowledge that would be appreciated:
- Decomposition methods in mixed-integer linear programming (Benders, Dantzig-Wolfe, Lagrangian)
- Stochastic programming, robust optimization
- Dynamic programming
- Modeling or solving industrial problems

**Salary:** approximately 2,250 euros/month (net)


**Environment:**

The work will primarily be conducted in the Inria project-team Edge, in the building of the
Mathematics Institute of Bordeaux located at Talence.
There will also be a number of trips to EDF offices (Massy): one to two weeks at the start of the position
and approximately 2 days per month. There may also be one or two trips to Lille. Transport and
accommodation costs for these trips are covered.

**References:**

Atakan, S. and Sen, S. (2018). A progressive hedging based branch-and-bound algorithm for mixed-integer
stochastic programs. _Computational Management Science_, 15(3-4):501–540.

Blanchot, X., Clautiaux, F., Detienne, B., Froger, A., and Ruiz, M. (2023). The benders by batch algorithm: de-
sign and stabilization of an enhanced algorithm to solve multicut benders reformulation of two-stage stochastic
programs. _European Journal of Operational Research_, 309(1):202–2016.

Chen, R. and Luedtke, J. (2022). On generating lagrangian cuts for two-stage stochastic integer programs.
INFORMS Journal on Computing, 34(4):2332–2349.

Kim, K. and Dandurand, B. (2022). Scalable branching on dual decomposition of stochastic mixed-integer
programming problems. _Mathematical Programming Computation_, 14(1):1–41.

Kim, K. and Zavala, V. M. (2018). Algorithmic innovations and software for the dual decomposition method
applied to stochastic mixed-integer programs. _Mathematical Programming Computation_, 10(2):225–266.

Lara, C. L., Siirola, J. D., and Grossmann, I. E. (2020). Electric power infrastructure planning under uncer-
tainty: stochastic dual dynamic integer programming (sddip) and parallelization scheme. _Optimization and
Engineering_, 21(4):1243–1281.

Rahmaniani, R., Crainic, T. G., Gendreau, M., and Rei, W. (2017). The benders decomposition algorithm: A
literature review. _European Journal of Operational Research_, 259(3):801–817.

Zou, J., Ahmed, S., and Sun, X. A. (2019). Stochastic dual dynamic integer programming. _Mathematical
Programming_, 175:461–502.
