---
title: CATIE
show_title: false
tags: [ collaboration , clautiaux , froger, yan ]
subtitle: Machine learning and optimization  # optional
tag: catie # unique tag of this project
# layout: single_collaboration

# optional parameters
image: ../resources/catie.webp     # optional
imagesize: 70
date:          # optional
weight: 10
consortium: Edge, CATIE
funding: CATIE + ANRT (CIFRE)
duration: 2023 - 2026
---

CATIE (Centre Aquitain des Technologies de l’Information et Électroniques) is a non-profit organization created in 2014 based in the Région Nouvelle-Aquitaine. As a technology resources center specialized in digital technology, its main mission is to support SMEs and intermediate size companies in their digital transformation and to help them embracing and integrating related technologies.

Our collaboration with CATIE is about machine learning and optimization. We focus on problems related to shortest path problem with side constraints. 


<!--more-->

People involved: 
- Boris Albar (CATIE) 
- [François Clautiaux]({{<  ref "team-members/clautiaux.md"  >}})
- [Aurélien Froger]({{<  ref "team-members/froger.md"  >}})
- [Fulin Yan]({{<  ref "team-members/yan.md"  >}})

---

{{< related_posts max=3 >}}
