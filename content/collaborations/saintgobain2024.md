---
title: Saint Gobain
show_title: false
tags: [ collaboration, clautiaux, arslan, pierre-pinet]
subtitle: "Routing problem under uncertainty"  # optional
tag: saint-gobain # unique tag of this project
# layout: single_collaboration

# optional parameters
image: ../resources/sgr.webp     # optional
imagesize: 50
date:        # optional
weight:
consortium: EDGE Saint-Gobain Recherche
funding: Saint Gobain + ANRT (CIFRE)
duration: 2025 - 2028
active : true
---

Saint-Gobain Research Paris is an industrial research and development centre working for light and sustainable construction of the Saint-Gobain Group, the world leader in light and sustainable construction.

The collaboration is centered around the PhD thesis of Pierre Pinet. 


<!--more-->

__People involved:__
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- [Ayse Arslan]({{< ref "/team-members/arslan" >}}) (Edge)
- [Pierre Pinet]({{< ref "/team-members/pierre-pinet" >}}) (Edge, St Gobain)

---

{{< related_posts max=3 >}}
