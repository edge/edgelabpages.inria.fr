---
title: Renault
show_title: false
tags: [ collaboration , clautiaux]
# subtitle: My collaboration subtitle  # optional
tag: renault # unique tag of this project
layout: single_collaboration
# optional parameters
image: ../../resources/renault.webp     # optional
imagesize: 40
date:         # optional
weight: 10
description: 
consortium: EDGE, Renault
funding: France Relance
duration: 2022
active : false
---

Renault is a French multinational automobile manufacturer established in 1899. The company produces a range of cars and vans and in the past, has manufactured trucks, tractors, tanks, buses/coaches, aircraft and aircraft engines, and autorail vehicles. 

<!--more-->

The objective of this project is to optimize the configuration and production of prototype cars. This problem occurs during the testing phase of a new vehicle. It involves determining all the variants of the vehicle to be manufactured in order to carry out these tests, and scheduling these tests over time. We proposed a formal definition as a scheduling problem, proved some useful properties, and designed an efficient algorithm based on column-generation to solve it. 

People involved: 
- [François Clautiaux]({{<  ref "team-members/clautiaux.md"  >}})
- Siham Essodaigui (Renault)
- Alain Nguyen (Renault)
- Ruslan Sadykov (Atoptima, formerly EDGE)
- Nawel Younes (Renault)

---

{{< publications hal_id="04185248" >}}


{{< related_posts max=3 >}}
