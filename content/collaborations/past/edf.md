---
title: EDF
show_title: false
tags: [ collaboration, edf, detienne ]
subtitle: "Combining Dantzig-Wolfe and Benders decompositions to solve a large-scale nuclear outage planning problem"  # optional
tag: edf-nuclear-outage-planning # unique tag of this project
# layout: single_collaboration

# optional parameters
image: ../../resources/edf.webp     # optional
imagesize: 60
date:          # optional
consortium: RealOpt, EDF
funding: EDF + ANRT (CIFRE)
duration: 2014 - 2018
active : false
---

EDF is the major French multinational electric utility company owned by the French state. 

Optimizing nuclear unit outages is of significant economic importance for the French electricity company EDF, 
as these outages induce a substitute production by other more expensive means to fulfill electricity demand. 
This problem is quite challenging given the specific operating constraints of nuclear units, the stochasticity of both the demand and non-nuclear units availability, 
and the scale of the instances. 
To tackle these difficulties we used a combined decomposition approach. 
The operating constraints of the nuclear units were built into a Dantzig-Wolfe pricing subproblem whose solutions define the columns of a demand covering formulation. 
The scenarios of demand and non-nuclear units availability are handled in a Benders decomposition. 
Our approach is shown to scale up to the real-life instances of the French nuclear fleet. 


<!--more-->

__People involved:__
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Edge)
- Rodolphe Griset (EDF/RealOpt)
- François Vanderbeck  (RealOpt)
- Pascale Bendotti (EDF)
- Marc Porcheron (EDF)


---

{{< related_posts max=3 >}}
