---
title: Saint Gobain
show_title: false
tags: [ collaboration, clautiaux, sadykov]
subtitle: "Two-dimensional cutting problems with defects"  # optional
tag: saint-gobain # unique tag of this project
# layout: single_collaboration

# optional parameters
image: ../../resources/sgr.webp     # optional
imagesize: 50
date:        # optional
weight:
consortium: RealOpt, Saint-Gobain Recherche
funding: Saint Gobain + ANRT (CIFRE)
duration: 2014 - 2018
active : false
---

Saint-Gobain Research Paris is an industrial research and development centre working for light and sustainable construction of the Saint-Gobain Group, the world leader in light and sustainable construction.

The collaboration was centered around the PhD thesis of Quentin Viaud. 


<!--more-->

__People involved:__
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Edge)
- Ruslan Sadykov
- Quentin Viaud

---

{{< related_posts max=3 >}}
