---
title: RTE
show_title: false
tags: [ collaboration, clautiaux, froger, detienne ]
subtitle: "Solving large-scale two-stage stochastic optimization programs: application to investment problems for power systems"  # optional
tag: rte # unique tag of this project

# optional parameters
image: ../../resources/rte.webp     # optional
imagesize: 32
date:      # optional
weight:
consortium: EDGE, RTE
funding: RTE + ANRT (CIFRE)
duration: 2018-2021
active : false
---

RTE is France’s Transmission System Operator. It is in charge of the high and ultra-high voltage electricity transmission network in France and of the electricity exchanges with the neighbouring countries. Its main role is to guarantee in real time the balance between electricity production and consumption.


In 2019, a contract was signed with RTE for a PhD on algorithms to speedup Benders' decomposition. The PhD student was Xavier Blanchot under the supervision of François Clautiaux and Aurélien Froger.

<!--more-->

People involved:
- [Xavier Blanchot]({{<  ref "team-members/past/blanchot.md"  >}})  (RTE, Realopt) 
- [François Clautiaux]({{<  ref "team-members/clautiaux.md"  >}})  (Realopt)
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Realopt)
- [Aurélien Froger]({{<  ref "team-members/froger.md"  >}})  (Realopt)
- Manuel Ruiz (RTE)

---

{{< related_posts max=3 >}}
