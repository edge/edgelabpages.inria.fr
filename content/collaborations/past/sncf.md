---
title: SNCF
show_title: false
tags: [ collaboration, clautiaux, detienne]
subtitle: "Rolling Stock Rotation Planning and Train Selection"  # optional
tag: sncf # unique tag of this project
# layout: single_collaboration

# optional parameters
image: ../../resources/sncf.webp     # optional
imagesize: 40
date:         # optional
weight:
consortium: Edge, SNCF
funding: SNCF + ANRT (CIFRE)
duration: 2016 - 2019
active : false
---

SNCF is the French Railway operator. This project was funded by TER Nouvelle Aquitaine.  

This collaboration funded the PhD thesis of Mohamed Benkirane. The subject of the thesis was an integrated optimization approach for timetable and rolling stock rotations planning in the context of passenger railway traffic. Our approach was based on a time-space hyper-graph model, which can handle trains composed by multiple self-powered railcars on part of their paths. 


<!--more-->

People involved: 

- Mohamed Benkirane (SNCF, Realopt)
- [François Clautiaux]({{< ref "/team-members/clautiaux" >}}) (Realopt)
- Jean Damay (SNCF)  
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Realopt)



---

{{< related_posts max=3 >}}
