---
title: Orange
show_title: false
tags: [ collaboration ]
subtitle: Network design  # optional
tag: orange # unique tag of this project

# optional parameters
image: ../resources/orange.webp    # optional
imagesize: 70
date:         # optional
weight: 10
consortium: EDGE, Orange
funding: Orange + ANRT (CIFRE)
duration: 2023-2024
active : false
---

Orange is a French multinational telecommunications corporation. The company specialises in mobile, landline, internet and Internet Protocol television (IPTV) services since 2006.

In 2023, a contract was signed with Orange for a PhD on network optimization. The PhD student is John Jairo Quiroga Orozco under the supervision of Boris Detienne and Pierre Pesneau.

<!--more-->

The objective is 

People involved: 
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}})
- Nancy Perrot (Orange)
- [Pierre Pesneau]({{<  ref "team-members/pesneau.md"  >}})
- [John J. Quiroga O.]

---

{{< related_posts max=3 >}}
