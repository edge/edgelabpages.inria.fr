---
title: EDF
show_title: false
tags: [ collaboration, edf, arslan, detienne, froger, sangare]
subtitle: "EDF-Inria Challenge - Generation expansion planning problems"  # optional
tag: defi-inria-edf # unique tag of this project
# layout: single_collaboration

# optional parameters
image: ../resources/edf.webp     # optional
imagesize: 60
date:       # optional
weight: 10
consortium: EDGE, Inocs, EDF
funding: Inria + EDF
duration: 2023 - 2026
active : true

summary: EDF is the major French multinational electric utility company owned by the French state. Our collaboration is part of the EDF-Inria Challenge, and specifically a project titled  "_Decomposition algorithms for solving generation expansion planning problems_".

---

Multiple problems studied by the ”long-term planning” team in the OSIRIS department of EDF R&D correspond to making investment decisions over a multi-year planning horizon while taking
into account economic, physical or legal restrictions and grid balancing.

By their nature, these problems can be modeled as multi-stage stochastic optimization problems in the
sense that it is possible to adapt the investment decisions to the realization of uncertainty over the course
of the planning horizon. The requirement to balance the electricity grid results in Unit Commitment (UC)
problems appearing as subproblems. The presence of numerous discrete decisions is a notable characteristic
of these problems.

The complexity and the scale of these problems lead EDF to resort to simplifying assumptions in their
solution (for instance, multi-stage problems are approximated by two-stage ones or mixed-integer linear
problems are approximated by linear problems). Currently, a tool named TiLT (”Long-Term Investment
Trajectories”) is used to optimize investment decisions with a multi-year vision where ”trajectories” are
used to formulate very large-scale linear models (with millions of variables and constraints). A heuristic
”Investment Loop” is used to fix the investment decisions based on a ”Unit Commitment” (UC) solver
called Continental. The team has started working on the solution of the two-stage (meaning that all invest-
ment decisions are taken before the realization of uncertainty) approximation of these problems exploiting
the fact that when investment decisions are fixed the constraints ensuring the electricity grid balance can
be decomposed into independent subproblems by trajectory. Geographical and temporal decoupling is
also used to reduce the size of sub-problems. Decomposition methods - notably Benders or Lagrangian
decomposition - can be used to exploit this type of structure. In a Benders decomposition approach, the
master problem consists of fixing the investment decisions (related to the electricity generation plants)
and subproblems reduce to UC problems.

Solving the targeted investment problems with finer approximations is an important problematic for
obtaining more judicious investment decisions. Taking into account the discrete nature of certain decisions
(for instance switch-on costs of plants, ramping constraints, minimum on/off times, minimum power etc.)
and the stochastic and multi-stage nature of these problems represent technical barriers. In order to
overcome this challenge it is necessary to develop advanced algorithmic techniques based on reformulation
and decomposition algorithms. In order to be efficient, these techniques will also have to undergo a number
of improvements.


__People involved__: 
- [Ayse Arslan]({{<  ref "team-members/froger.md"  >}})  (Edge)
- [Boris Detienne]({{<  ref "team-members/detienne.md"  >}}) (Edge)
- Bernard Fortz (Inocs)
- [Aurélien Froger]({{<  ref "team-members/froger.md"  >}})  (Edge)
- Rodolphe Griset (EDF)
- Cecile Rottner (EDF)
- [Mariam Sangare]({{<  ref "team-members/sangare.md"  >}})  (Postdoc, Edge)

---

{{< related_posts max=3 >}}
