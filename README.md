[![pipeline status](https://gitlab.inria.fr/edge/edge.gitlabpages.inria.fr/badges/main/pipeline.svg)](https://gitlab.inria.fr/edge/edge.gitlabpages.inria.fr)
---

Edge Hugo website using GitLab Pages.

https://edge.gitlabpages.inria.fr/


# Documentation

[How to use Hugo and add new content](content/README.md)
